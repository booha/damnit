package es.vged.afd.utils;

public final class UrlAPIConstants {

	public static final int KO_CODE = 0;
	public static final int OK_CODE = 1;

	public static final String API_VERSION = "/v1";
	
	public static final String RESOURCE = "/resource1/";

	public static final String ID_VAR = "/{id}";

	public static final String SECTION_RESOURCE = "/sections";

	public static final String CHANNEL_RESOURCE = "/channel";

	public static final String BENEFITS_RESOURCE = "/benefit";

	public static final String ARGUMENTS_RESOURCE = "/argument";

	public static final String COVERAGES_RESOURCE = "/coverage";

	public static final String SIMULATOR_RESOURCE = "/simulator";

	public static final String INSTALLATION_RESOURCE = "/installation";
	public static final String INSTALLATION_PRICE_MESSAGE = "/message";

	public static final String MODEL_RESOURCE = "/model";

	public static final String ANNUALKM_RESOURCE = "/annualkm";

	public static final String ENGAGEMENT_RESOURCE = "/engagement";

	public static final String USER_RESOURCE = "/user";	

	public static final String USER_LAST_DATE_ACCESS_RESOURCE = "/lastdateaccess";

	public static final String MODEL_VAR = "/{model}";

	public static final String PERIOD_VAR = "/{period}";

	public static final String CONTRACT_VAR = "/{contract}";

	public static final String KM_VAR = "/{km}";

	public static final String USERNAME_VAR = "/{username}";
	
	public static final String NAME = "/{name}";
	
	public static final String PROFILE_ROOT = "/profile";
	
	public static final String DEALER_ROOT = "/dealer";

	public static final String ALL = "/all";
	
	public static final String AUTHENTICATION_ROOT = "/authentication";
	public static final String AUTHENTICATION_TOKEN_LOGIN = "/getTokenLogin";
	public static final String AUTHENTICATION_LOGOUT = "/logout";
	public static final String AUTHENTICATION_CREATE_TOKEN = "/getToken";
	public static final String INSTALLATION_CODE_VAR = "/{installationCode}";
	public static final String USER_INSTALLATION = "/userInstallation";
	
	private UrlAPIConstants() {}

}
