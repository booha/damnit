package es.vged.afd.service;

import es.vged.afd.data.GenericInput;

public interface EngagementInterface {

	StandardService getEngagementByChannel(GenericInput genericInput);

}