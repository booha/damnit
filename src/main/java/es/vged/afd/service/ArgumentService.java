package es.vged.afd.service;

import java.util.List;
import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericInput;
import es.vged.afd.repository.ArgumentRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class ArgumentService implements ArgumentInterface {

 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject
	ArgumentRepository argumentRepository;
	
	/* (non-Javadoc)
	 * @see es.vged.afd.service.ArgumentInterface#getAllArguments(es.vged.afd.data.GenericInput)
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllArguments(GenericInput genericInput){
		StandardService response = new StandardService();
		
		List<?> arguments = argumentRepository.findByChannelId(genericInput.getId());
		
		if(arguments.isEmpty()) {
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("No fields found");
			response.setData(arguments);
		}
		
		else {
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(arguments);
		}
		
		return response;
	}
}
