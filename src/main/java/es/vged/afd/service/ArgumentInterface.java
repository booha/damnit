package es.vged.afd.service;

import es.vged.afd.data.GenericInput;

public interface ArgumentInterface {

	StandardService getAllArguments(GenericInput genericInput);

}