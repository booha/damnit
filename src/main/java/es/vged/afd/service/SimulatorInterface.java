package es.vged.afd.service;

import es.vged.afd.data.SimulatorInput;

public interface SimulatorInterface {

	/*
	 * getSimulatorCalculations: Realiza la simulación de cálculos para el ahorro a
	 * largo plazo(120 meses) y a corto plazo(24 meses) simulatorInput: Dto de tipo
	 * input que almacena model, tipo de contrato, km/anual y codigo instalación
	 */
	StandardService simCalculs(SimulatorInput simulatorInput, String installationCode);

	/*
	 * getSimulatorCalculations: Realiza la simulación de cálculos para el ahorro a
	 * largo plazo(120 meses) y a corto plazo(24 meses) simulatorInput: Dto de tipo
	 * input que almacena model, tipo de contrato, km/anual para la instalación por defecto del usuario
	 */
	@Deprecated
	StandardService simCalculs(SimulatorInput simulatorInput);
}