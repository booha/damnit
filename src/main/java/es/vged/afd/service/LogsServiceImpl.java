package es.vged.afd.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.LogsInput;
import es.vged.afd.data.model.LogsEntity;
import es.vged.afd.repository.LogsRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class LogsServiceImpl implements LogsService {
	
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject
	private LogsRepository logsRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(LogsServiceImpl.class);

	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService insertLoginsHistory(LogsInput logsInput) {

		StandardService response = new StandardService();

		try {

			if (logsInput.getIdUser() != null) {
				LogsEntity logsEntity;
				if (logsInput.getValue() != null) {

					logsInput.setLogType(2);
					logsEntity = convert(logsInput);
					logsRepository.save(logsEntity);
				} else {

					logsInput.setValue(null);
					logsInput.setLogType(1);
					logsEntity = convert(logsInput);
					logsRepository.save(logsEntity);
				}
			}

			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(null);

		} catch (Exception e) {

			logger.error("Esto no va", e);
		}
		return response;

	}

	protected LogsEntity convert(LogsInput logsInput) {

		LogsEntity logsEntity = new LogsEntity();

		logsEntity.setValue(logsInput.getValue());
		logsEntity.setLogType(logsInput.getLogType());

		logsEntity.setIdUser(logsInput.getIdUser());
		logsEntity.setIdInstallation(logsInput.getIdInstallation());

		return logsEntity;

	}

}