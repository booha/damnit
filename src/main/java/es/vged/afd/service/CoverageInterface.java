package es.vged.afd.service;

import es.vged.afd.data.GenericInput;

public interface CoverageInterface {

	StandardService getAllCoverages(GenericInput genericInput);

}