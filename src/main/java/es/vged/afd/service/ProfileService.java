package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.vged.springarch.responses.GenericResponse;
import com.vged.springarch.utils.CommonConstants;

import es.vged.afd.data.ProfileOutput;
import es.vged.afd.data.model.ProfileEntity;
import es.vged.afd.repository.ProfileRepository;

@Service
public class ProfileService implements ProfileInterface {

 	private static final String AUTH_MANAGE_USERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_USERS)";

	@Autowired
	ProfileRepository profileRepository;
	
	@Override
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<List<ProfileOutput>> getAllProfiles() {
		List<ProfileEntity> profileList = profileRepository.findAll();
		
		List<ProfileOutput> listOutput = new ArrayList<>();
		
		for (ProfileEntity profileItem : profileList) {
			ProfileOutput profileOutput = new ProfileOutput();
			profileOutput.setId(profileItem.getId());
			profileOutput.setName(profileItem.getName());
			
			listOutput.add(profileOutput);
		}
		
		return handleResponse(listOutput);
	}
	
	private <T> GenericResponse<T> handleResponse(T response) {
		if (response != null) {
			return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.SC_OK), CommonConstants.OK,
					response);
		} else {
			return new GenericResponse<>(CommonConstants.ERROR, String.valueOf(HttpStatus.SC_SERVICE_UNAVAILABLE),
					CommonConstants.SRVC_UNAVAIBLE_MESSAGE, null);
		}
	}

}
