package es.vged.afd.service;

import java.util.List;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.UserBean;
import es.vged.afd.data.UserOutputBean;

public interface UserInterface {

	GenericResponse<Boolean> createAndUpdateUser(UserBean userBean);

	GenericResponse<List<UserOutputBean>> getUsers(String name);

	GenericResponse<Boolean> deleteUser(Integer id);

}
