package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericInput;
import es.vged.afd.data.GenericOutput;
import es.vged.afd.data.model.EngagementEntity;
import es.vged.afd.repository.EngagementRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class EngagementService implements EngagementInterface {

 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject
	EngagementRepository engagementRepository;
	
	
	/* (non-Javadoc)
	 * @see es.vged.afd.service.EngagementInterface#getEngagementByChannel(es.vged.afd.data.GenericInput)
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getEngagementByChannel(GenericInput genericInput) {

		StandardService response = new StandardService();
		
		List<GenericOutput> result = new ArrayList<>();
		
		try {
			
			List<EngagementEntity> engagements = engagementRepository.findByChannelIdOrderByNameAsc(genericInput.getId());
			
			engagements.forEach( x -> result.add(converterToGenericOutput(x)) );
			
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(result);

		} catch (Exception e) { //NOSONAR //TODO:Refactor

			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("Error");
			response.setData(null);
		}

		return response;
		
	}


	private GenericOutput converterToGenericOutput(EngagementEntity engagements) {
		
		return new GenericOutput(
			engagements.getId(),
			engagements.getName()
		);
	}
}
