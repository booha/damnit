package es.vged.afd.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.SimulatorCalculationsOutput;
import es.vged.afd.data.SimulatorInput;
import es.vged.afd.data.SimulatorOutput;
import es.vged.afd.data.model.AnnualkmEngagementEntity;
import es.vged.afd.data.model.InstallationEntity;
import es.vged.afd.data.model.ParameterEntity;
import es.vged.afd.data.model.PlanEngagementEntity;
import es.vged.afd.data.model.SimulatorEntity;
import es.vged.afd.data.model.UserEntity;
import es.vged.afd.data.model.enumeration.AnnualKmEngagementFormulaEnum;
import es.vged.afd.repository.AnnualkmEngagementRepository;
import es.vged.afd.repository.InstallationRepository;
import es.vged.afd.repository.ParameterRepository;
import es.vged.afd.repository.PlanEngagementRepository;
import es.vged.afd.repository.SimulatorRepository;
import es.vged.afd.repository.UserRepository;
import es.vged.afd.rest.security.SecurityHelper;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class SimulatorService implements SimulatorInterface {

	private static final Logger logger = LoggerFactory.getLogger(SimulatorService.class.getName());
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	/*
	 * PENDIENTE: Implementar GenericResponse de la librería
	 * "import com.vged.springarch.responses.GenericResponse;" Implementar Logger
	 * 
	 */

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private SimulatorRepository simulatorRepository;

	@Autowired
	private ParameterRepository parameterRepository;

	@Autowired
	private PlanEngagementRepository planEngagementRepository;

	@Autowired
	private AnnualkmEngagementRepository annualkmEngagementRepository;

	@Autowired
	private InstallationRepository installationRepository;

	
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService simCalculs(SimulatorInput simulatorInput)
	{
		final String currentUserName = SecurityHelper.getPrincipalUsername();
		final UserEntity user = userRepository.findOneByUsername(currentUserName);
		return simCalculs(simulatorInput,user.getInstallation());
	}
	
	/*
	 * getSimulatorCalculations: Realiza la simulación de cálculos para el ahorro a
	 * largo plazo(120 meses) y a corto plazo(24 meses) simulatorInput: Dto de tipo
	 * input que almacena model, tipo de contrato y km/anual
	 */
	/* (non-Javadoc)
	 * @see es.vged.afd.service.SimulatorInterface#simCalculs(es.vged.afd.data.SimulatorInput, java.lang.String)
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService simCalculs(SimulatorInput simulatorInput, String installationCode) {
		return simCalculs(simulatorInput, installationRepository.findOneByInstallationCode(installationCode));		
	}
	
	
	private StandardService simCalculs(SimulatorInput simulatorInput, InstallationEntity installation) {
		List<SimulatorOutput> simulatorOutput = new ArrayList<>();
		
		/* 1. Obtenemos el precio medio de MO de la tabla de parámetros */
		final String parameterNameAveragePriceLabor = "AveragePriceLabor";
		final ParameterEntity parameter = parameterRepository.findByName(parameterNameAveragePriceLabor);

		Boolean bValidate = true; // NOSONAR //TODO:Refactor

		/*
		 * Por si en la tabla el valor esta seteado con ',' y no con '.' reemplazamos el
		 * string para poder convertirlo a double
		 */
		final String parameterValueAveragePriceLabor = parameter.getValue().replace(",", "."); // NOSONAR//TODO:Refactor

		Double parameterAveragePriceLabor = 0d;
		try {
			parameterAveragePriceLabor = Double.valueOf(parameterValueAveragePriceLabor);
		} catch (NumberFormatException e) {
			bValidate = false; // NOSONAR //TODO:Refactor
		}

		if (bValidate) {
			/*
			 * 2. Obtenemos el precio medio de MO de la tabla de la instalación a la que
			 * pertenece el usuario actual
			 */
			final String currentUserName = SecurityHelper.getPrincipalUsername();
			final UserEntity user = userRepository.findOneByUsername(currentUserName);

			bValidate = simulatorValidate(parameter, simulatorInput, user);

			if (bValidate) {

				try {

					final Double installationAveragePriceLabor = installation.getAveragePriceLabor();

					/*
					 * 3. Obtenemos los variables de cálculo correspondientes al km/anual y el tipo
					 * de contratación
					 */
					final AnnualkmEngagementEntity annualKmEngagement = annualkmEngagementRepository
							.findByAnnualkmIdAndEngagementId(simulatorInput.getAnnualKm(),
									simulatorInput.getEngagement());

					final List<PlanEngagementEntity> planEngagement = planEngagementRepository
							.getByEngagementIdAndOrderByPlanOrder(simulatorInput.getEngagement());

					for (final PlanEngagementEntity plan : planEngagement) { // NOSONAR //TODO:Refactor

						/*
						 * 4. Por cada Plan obtenemos los datos del simulador y realizamos los cálculos
						 */
						final SimulatorEntity simulator = simulatorRepository
								.findByPlanengagementAndModelIdAndAnnualkmId(plan, simulatorInput.getModel(),
										simulatorInput.getAnnualKm());

						SimulatorOutput itemSimulatorOutput;

						if (null != simulator) {

							/* 5. Cálculo de la MO a largo plazo */
							final Double laborLongConst = simulator.getLongCost() * simulator.getLaborPercentage()
									* installationAveragePriceLabor / parameterAveragePriceLabor
									+ simulator.getLongCost() * (1 - simulator.getLaborPercentage());
							/* 6. Cálculo de la MO a corto plazo */
							final Double laborShortConst = simulator.getShortCost() * simulator.getLaborPercentage()
									* installationAveragePriceLabor / parameterAveragePriceLabor
									+ simulator.getShortCost() * (1 - simulator.getLaborPercentage());

							/*
							 * 7. En función del tipo de FÓRMULA definida en el km/anual y tipo de contrato
							 * realizamos los cálculos del ahorro a largo y corto plazo
							 */
							SimulatorCalculationsOutput simulatorCalculations = calculateSave(simulator,
									annualKmEngagement, plan, laborLongConst, laborShortConst);

							itemSimulatorOutput = new SimulatorOutput(plan.getPlan().getId(), plan.getPlan().getName(),
									simulatorCalculations);

						} else {

							itemSimulatorOutput = new SimulatorOutput(plan.getPlan().getId(), plan.getPlan().getName(),
									null);

						}

						simulatorOutput.add(itemSimulatorOutput);

					}

				} catch (Exception e) { // NOSONAR //TODO:Refactor
					bValidate = false; // NOSONAR //TODO:Refactor
					logger.error(e.getMessage());
					logger.info("Error al realizar los cálculos: " + simulatorInput); // NOSONAR //TODO:Refactor
				}

			}
		}

		return getResponse(bValidate, simulatorOutput);

	}

	private StandardService getResponse(Boolean bValidate, List<SimulatorOutput> simulatorOutput) {
		StandardService response = new StandardService();

		if (!bValidate || simulatorOutput.isEmpty()) {

			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("No fields found");
			response.setData(null);

		} else {

			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(simulatorOutput);

		}

		return response;
	}

	private Boolean simulatorValidate(final ParameterEntity parameter, SimulatorInput simulatorInput,
			final UserEntity user) {
		Boolean bValidate = true; // NOSONAR //TODO:Refactor

		if (null == parameter || parameter.getValue().isEmpty()) {
			bValidate = false; // NOSONAR //TODO:Refactor
		}

		if (null == simulatorInput.getAnnualKm() || null == simulatorInput.getEngagement()
				|| null == simulatorInput.getModel()) {
			bValidate = false; // NOSONAR //TODO:Refactor
		}

		if ((bValidate) && (null == user || null == user.getInstallation().getAveragePriceLabor())) {
			bValidate = false; // NOSONAR //TODO:Refactor
		}

		return bValidate;

	}

	private SimulatorCalculationsOutput calculateSave(final SimulatorEntity simulator,
			final AnnualkmEngagementEntity annualKmEngagement, final PlanEngagementEntity plan,
			final Double laborLongConst, final Double laborShortConst) {

		Double longSaving = 0d;
		Double shortSaving = 0d;
		final DecimalFormat decimalFormat = new DecimalFormat(".##");

		/*
		 * En función del tipo de FÓRMULA definida en el km/anual y tipo de contrato
		 * realizamos los cálculos del ahorro a largo y corto plazo
		 */
		switch (AnnualKmEngagementFormulaEnum.valueOf(annualKmEngagement.getFormula())) {

		case SAVINGS_VN_NOFINANCIAL:
		case SAVINGS_PV:

			longSaving = simulator.getFeeAfd() * (annualKmEngagement.getLongTermVar() - plan.getMonthpromotion())
					- laborLongConst;
			shortSaving = simulator.getFeeAfd() * (annualKmEngagement.getShortTermVar() - plan.getMonthpromotion())
					- laborShortConst;

			break;

		case SAVINGS_VN_FINANCIAL: // NOSONAR //TODO:Refactor

			longSaving = plan.getCopayment()
					+ simulator.getFeeAfd()
							* (annualKmEngagement.getLongTermVar() - annualKmEngagement.getStartPaymentMonthly())
					- laborLongConst;
			shortSaving = plan.getCopayment() - laborShortConst;

			break;

		case SAVINGS_VO_NOFINANCIAL:

			longSaving = 0d;
			shortSaving = 0d;

			break;

		case SAVINGS_VO_FINANCIAL: // NOSONAR //TODO:Refactor

			longSaving = plan.getCopayment()
					+ (annualKmEngagement.getLongTermVar() - annualKmEngagement.getCarAge() - simulator.getMonthCar())
							* simulator.getFeeAfd()
					- laborLongConst;
			shortSaving = plan.getCopayment() - laborShortConst;

			break;

		}

		return new SimulatorCalculationsOutput(simulator.getFeeAfd(), simulator.getLongCost(), simulator.getShortCost(),
				simulator.getMonthCar(), plan.getCopayment(), decimalFormat.format(longSaving),
				decimalFormat.format(shortSaving));

	}

}
