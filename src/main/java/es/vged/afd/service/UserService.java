package es.vged.afd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vged.springarch.responses.GenericResponse;
import com.vged.springarch.utils.CommonConstants;

import es.vged.afd.data.LogsInput;
import es.vged.afd.data.UserBean;
import es.vged.afd.data.UserInput;
import es.vged.afd.data.UserInstallationOutput;
import es.vged.afd.data.UserOutputBean;
import es.vged.afd.data.mapper.DealerMapper;
import es.vged.afd.data.mapper.InstallationMapper;
import es.vged.afd.data.mapper.ProfileMapper;
import es.vged.afd.data.model.DealerEntity;
import es.vged.afd.data.model.InstallationEntity;
import es.vged.afd.data.model.ProfileAuthorityEntity;
import es.vged.afd.data.model.ProfileEntity;
import es.vged.afd.data.model.UserEntity;
import es.vged.afd.data.model.UserProfileEntity;
import es.vged.afd.repository.DealerRepository;
import es.vged.afd.repository.InstallationRepository;
import es.vged.afd.repository.ProfileAuthorityRepository;
import es.vged.afd.repository.ProfileRepository;
import es.vged.afd.repository.UserProfileRepository;
import es.vged.afd.repository.UserRepository;
import es.vged.afd.rest.security.SecurityHelper;
import es.vged.afd.utils.UrlAPIConstants;


@Service
public class UserService implements UserInterface {

 	private static final String AUTH_MODIFY_LABOR_PRICE = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MODIFY_LABOR_PRICE)";
	private static final String AUTH_MANAGE_USERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_USERS)";
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";


	@Autowired
	private UserRepository userRepository;

	@Autowired
	private InstallationRepository installationRepository;
	
	@Autowired
	private ProfileAuthorityRepository profileAuthorityRepository;
	
	@Autowired
	private LogsService logsService;
	
	@Autowired
	private DealerRepository dealerRepository;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired 
	private UserProfileRepository userProfileRepository;
	
	@Autowired
	private ProfileMapper profileMapper;
	
	@Autowired
	private DealerMapper dealerMapper;
	
	@Autowired
	private InstallationMapper installationMapper;

	
	public StandardService getUserInstallation(String username) {

		StandardService response = new StandardService();

		final UserEntity currentUser = userRepository.findOneByUsername(username);

		if (null == currentUser) {

			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("No fields found");
			response.setData(null);

		} else {

			UserProfileEntity userProfile = userProfileRepository.findByPkUserId(currentUser.getId());
			
			ProfileEntity profile = profileRepository.findOne(userProfile.getProfileEntity().getId());
			
			List<ProfileAuthorityEntity> profileAuthList = profileAuthorityRepository.findByPkProfileId(profile.getId());
			
			List<String> authList = new ArrayList<>();
			
			for(ProfileAuthorityEntity profileAuthItem : profileAuthList) {				
				authList.add(profileAuthItem.getAuthority().getName());
			}
			
			final UserInstallationOutput userInstallationData = new UserInstallationOutput(currentUser.getUsername(),
					currentUser.getUserCompleteName(), currentUser.getInstallation().getInstallationCode(),
					currentUser.getInstallation().getBusinessName(),
					currentUser.getInstallation().getAveragePriceLabor(),
					profile.getName(), authList);

			List<UserInstallationOutput> userInstallationOutput = new ArrayList<>();
			userInstallationOutput.add(userInstallationData);

			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(userInstallationOutput);

		}

		return response;

	}
	
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService updateLastDateAccess(UserInput userInput) {

		final String username = userInput.getUsername();

		StandardService response = new StandardService();

		try {

			Date currentDate = new Date();

			userRepository.updateLastDateAccess(currentDate, username);

			// hacer log aquí
			UserEntity user = userRepository.findOneByUsername(username);
			LogsInput logsInput = new LogsInput();
			logsInput.setIdUser(user.getId());
			logsInput.setIdInstallation(user.getInstallation().getInstallationCode());
			logsService.insertLoginsHistory(logsInput);

			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(null);

		} catch (Exception e) { // NOSONAR //TODO:Refactor
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("Error");
			response.setData(null);
		}

		return response;
	}

	@PreAuthorize(value = AUTH_MODIFY_LABOR_PRICE)
	public StandardService checkUserInstallation() {
		StandardService response = new StandardService();

		response.setCode(0);
		response.setMessage("Error!");
		UserEntity user = userRepository.findOneByUsername(SecurityHelper.getPrincipalUsername());
		response.setCode(0);
		response.setMessage("Success!");
		List<InstallationEntity> installations = installationRepository
				.findInstallationByDealerCode(user.getDealerCode());

		if (installations == null || installations.isEmpty()) {
			response.setCode(2);
		} else {

			for (InstallationEntity installation : installations) {
				if (installation.getAveragePriceLabor() == null) {
					response.setCode(1);
				}
			}
		}
		
		return response;
	}

	@Override
	@Transactional
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<Boolean> createAndUpdateUser(UserBean userBean) {
		
		Boolean result;
		
		if(userBean.getId() == null) {
			UserEntity user = new UserEntity();
			insertUser(userBean, user);
		} else {
			UserEntity user = userRepository.findOne(userBean.getId().intValue());
			insertUser(userBean, user);
		}
		
		result = Boolean.TRUE;
		
		return handleResponse(result);

	}

	private void insertUser(UserBean userBean, UserEntity user) {
		DealerEntity dealer = dealerRepository.findOne(userBean.getDealer());
		ProfileEntity profile = profileRepository.findOne(userBean.getRole());

		InstallationEntity installation = installationRepository.findOneByInstallationCode(userBean.getInstallationCode());
		
		user.setUserCompleteName(userBean.getName());
		user.setUsername(userBean.getUserName());
		user.setDealer(dealer);
		user.setDealerCode(dealer.getDealerCode());
		user.setInstallation(installation);
		userRepository.saveAndFlush(user);
		
		createUserProfile(user, profile);
	}

	private void createUserProfile(UserEntity user, ProfileEntity profile) {
		UserEntity userEntity = userRepository.findOneByUsername(user.getUsername());
		
		int id = user.getId();
		if(userProfileRepository.findByPkUserId(user.getId()) != null) {
			userProfileRepository.deleteByPkUserId(id);
		}
		
		UserProfileEntity userProfile = new UserProfileEntity();
		userProfile.setProfileEntity(profile);
		userProfile.setUserEntity(userEntity);
		userProfileRepository.saveAndFlush(userProfile);
	}

	@Override
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<List<UserOutputBean>> getUsers(String name) {
				
		List<UserEntity> userList = userRepository.getUsersByUserName(name);
		
		List<UserOutputBean> userOutputList = new ArrayList<>();

		for(UserEntity userItem : userList) {
			UserOutputBean userOutputBean = new UserOutputBean();
			userOutputBean.setUserName(userItem.getUsername());
			userOutputBean.setName(userItem.getUserCompleteName());
			userOutputBean.setInstallation(installationMapper.map(userItem.getInstallation()));
			userOutputBean.setId(userItem.getId());
			userOutputBean.setDealer(dealerMapper.map(userItem.getDealer()));
			userOutputBean.setRole(profileMapper.map(userItem.getProfilesList().get(0)));
			
			userOutputList.add(userOutputBean);
		}
		
		return handleResponse(userOutputList);
		
	}
	
	private <T> GenericResponse<T> handleResponse(T response) {
		if (response != null) {
			return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.SC_OK), CommonConstants.OK,
					response);
		} else {
			return new GenericResponse<>(CommonConstants.ERROR, String.valueOf(HttpStatus.SC_SERVICE_UNAVAILABLE),
					CommonConstants.SRVC_UNAVAIBLE_MESSAGE, null);
		}
	}

	@Override
	@Transactional
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<Boolean> deleteUser(Integer id) {
		
		UserEntity user = userRepository.findOne(id);
		
		if(user != null) {
			userProfileRepository.deleteByPkUserId(id);
			userRepository.deleteById(id);
		}
		
		return handleResponse(Boolean.TRUE);
	}
	
}
