package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.vged.springarch.responses.GenericResponse;
import com.vged.springarch.utils.CommonConstants;

import es.vged.afd.data.DealerOutput;
import es.vged.afd.data.model.DealerEntity;
import es.vged.afd.repository.DealerRepository;

@Service
public class DealerService implements DealerInterface {
	
	private static final String AUTH_MANAGE_USERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_USERS)";

	@Autowired
	DealerRepository dealerRepository; 
	
	@Override
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<List<DealerOutput>> getAllDealers() {
		List<DealerEntity> dealerList = dealerRepository.findAll();
		
		List<DealerOutput> listOutput = new ArrayList<>();
		
		for (DealerEntity dealerItem : dealerList) {
			DealerOutput dealerOutput = new DealerOutput();
			dealerOutput.setId(dealerItem.getId());
			dealerOutput.setName(dealerItem.getName());
			dealerOutput.setDealerCode(dealerItem.getDealerCode());
			
			listOutput.add(dealerOutput);
		}
		
		return handleResponse(listOutput);
	}

	private <T> GenericResponse<T> handleResponse(T response) {
		if (response != null) {
			return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.SC_OK), CommonConstants.OK,
					response);
		} else {
			return new GenericResponse<>(CommonConstants.ERROR, String.valueOf(HttpStatus.SC_SERVICE_UNAVAILABLE),
					CommonConstants.SRVC_UNAVAIBLE_MESSAGE, null);
		}
	}
	
}
