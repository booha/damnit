package es.vged.afd.service;

import java.util.List;

public class StandardService {
	
	private int code;
	
	private String message;
	
	private List<?> data;

	public StandardService() { super(); }

	public StandardService(int code, String message, List<?> data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() { return code; }

	public void setCode(int code) { this.code = code; } //NOSONAR //TODO:Refactor

	public String getMessage() { return message; }

	public void setMessage(String message) { this.message = message; } //NOSONAR //TODO:Refactor

	public List<?> getData() { return data; } //NOSONAR //TODO:Refactor

	public void setData(List<?> data) { this.data = data; } //NOSONAR //TODO:Refactor

	@Override
	public String toString() {
		return "StandardService [code=" + code + ", message=" + message + ", data=" + data + "]";
	}
	
}
