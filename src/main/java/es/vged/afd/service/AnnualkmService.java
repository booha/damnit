package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericOutput;
import es.vged.afd.data.model.AnnualkmEntity;
import es.vged.afd.repository.AnnualkmRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class AnnualkmService implements AnnualkmInterface {
	
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";
	private static final Logger logger = LoggerFactory.getLogger(AnnualkmService.class.getName());
	
	@Inject
	AnnualkmRepository annualkmRepository;
	
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllAnnualKm() {

		StandardService response = new StandardService();

		List<GenericOutput> result = new ArrayList<>();
		
		try {

			final List<AnnualkmEntity> annualkms = annualkmRepository.findAllByOrderByNameAsc();

			annualkms.forEach( x -> result.add(converterToGenericOutput(x)) );
			
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(result);

		} catch (Exception e) { //NOSONAR //TODO:Refactor
			logger.error(e.getMessage());
			
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("Error");
			response.setData(null);
		}

		return response;
	}

	
	private GenericOutput converterToGenericOutput(AnnualkmEntity annualkm) {
		
		return new GenericOutput(
			annualkm.getId(),
			annualkm.getName()
		);
	}
}