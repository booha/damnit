package es.vged.afd.service;

import java.util.List;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.ProfileOutput;


public interface ProfileInterface {

	GenericResponse<List<ProfileOutput>> getAllProfiles();

}
