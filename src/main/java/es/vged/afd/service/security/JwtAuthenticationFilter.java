package es.vged.afd.service.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.stereotype.Component;

import es.vged.afd.common.audit.LoggerHelper;
import es.vged.afd.controllers.security.HeaderConstants;
import es.vged.afd.rest.security.SecurityHelper;

@Component("jwtAuthenticationFilter")
public class JwtAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter
{

	private static final LoggerHelper MYLOGGER = LoggerHelper.getLoggerHelper(JwtAuthenticationFilter.class);
	private static final String CREDENTIAL_NO_APPLICABLE = "N/A";
	@Override
	@Autowired
	public void setAuthenticationManager(AuthenticationManager authenticationManager)
	{
		super.setAuthenticationManager(authenticationManager);
	}
	
	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request)
	{
		String userToken = null;
		try {
			userToken = request.getHeader(HeaderConstants.USER_TOKEN);

			MYLOGGER.info("***************** USER JWT " + userToken);
			UserJWT userJWT = SecurityHelper.validateToken(userToken);
			// Verifico al autenticidad del USER_TOKEN. 
			if(userJWT == null) 
			{
				return null;
			}

		} catch(Exception e) //NOSONAR //TODO:Refactor
		{	
			userToken = null;
			MYLOGGER.error(e.getMessage());
		}
		return userToken;			
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request)
	{
		MYLOGGER.info("Retrieving the Pre-Authenticated Credentials");
		return CREDENTIAL_NO_APPLICABLE;
	}
}