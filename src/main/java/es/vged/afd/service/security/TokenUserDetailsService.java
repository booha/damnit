package es.vged.afd.service.security;

import javax.inject.Inject;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import es.vged.afd.data.model.UserEntity;
import es.vged.afd.repository.UserRepository;
import es.vged.afd.rest.security.SecurityHelper;

/**
 * @see org.springframework.security.core.userdetails.UserDetailsService
 */
public class TokenUserDetailsService implements UserDetailsService
{
	private static final String PASS_NO_APPLICABLE = "N/A";

	@Inject
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String userToken)
	{
		UserJWT userJWT = SecurityHelper.validateToken(userToken);
		
		UserEntity user = userRepository.findOneByUsername(userJWT.getUsername());
		if (user == null)
		{
			throw new BadCredentialsException("User not found in database:" + userJWT.getUsername());
		}
		
		// guardamos al usuario en el contexto de Spring Security obteniendo sus permisos
		//return new org.springframework.security.core.userdetails.User(userToken, PASS_NO_APPLICABLE, userJWT.getGrantedAuthorities());
		return userJWT;
	}
}