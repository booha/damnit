package es.vged.afd.service.security;


public interface AuthenticationService
{
	String createToken(String username);
}