package es.vged.afd.service.security;

import javax.inject.Inject;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.vged.afd.data.model.UserEntity;
import es.vged.afd.repository.UserRepository;
import es.vged.afd.rest.security.SecurityHelper;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	@Inject
	private UserRepository userRepository;

	@Override
	@Transactional(readOnly = true)
	public String createToken(String ssoToken) {
		/**
		 * Si el token de sso no es de test, los validamos.
		 * 
		 * Usamos un token del SSO de test, porque expiran cada 30 minutos.
		 */

		String userName = SecurityHelper.getUserFromSsoToken(ssoToken);

		// Miramos si existe el usuario en la BD.
		UserEntity user = userRepository.findOneByUsername(userName);
		if (user == null) {
			throw new BadCredentialsException("User not found in database:" + userName);
		}

		return SecurityHelper.createToken(user);
	}
}
