package es.vged.afd.service.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import es.vged.afd.data.model.InstallationEntity;

public class UserJWT implements UserDetails{
	private static final long serialVersionUID = -3190672598294964182L;
	private Long id;
 	private String username;
 	private String name;
	private List<GrantedAuthority> grantedAuthorities;
	
 	private InstallationEntity installation;

 	
	public UserJWT(Long id, String username, List<GrantedAuthority> grantedAuthorities) {
		super();
		this.id = id;
		this.username = username;
		this.grantedAuthorities = grantedAuthorities;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InstallationEntity getInstallation() {
		return installation;
	}

	public void setInstallation(InstallationEntity installation) {
		this.installation = installation;
	}
	
	
	@Override
	public String toString() {
		return "UserJWT [id=" + id + ", userName=" + username + ", name=" + name 
				+ ", grantedAuthorities=" + grantedAuthorities + ", installation=" + installation + "]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}