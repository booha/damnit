package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.ModelOutput;
import es.vged.afd.data.model.ModelEntity;
import es.vged.afd.repository.ModelRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class ModelService implements ModelInterface {
	
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Autowired
	ModelRepository modelRepository;
	
	/* (non-Javadoc)
	 * @see es.vged.afd.service.ModelInterface#getAllModels()
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllModels() {

		StandardService response = new StandardService();
		
		List<ModelOutput> result = new ArrayList<>();

		try {

			final List<ModelEntity> models = modelRepository.findAll();
	
			models.forEach( x -> result.add(converterToModelOutput(x)) );
			
			
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(result);
			
		} catch (Exception e) { //NOSONAR //TODO:Refactor

			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("Error");
			response.setData(null);
		}
		
		return response;
	}
	
	
	private ModelOutput converterToModelOutput(ModelEntity model) {
		
		return new ModelOutput(
			model.getModelTypeEntity().getName(),
			model.getId(),
			model.getName(),
			model.getModelTypeEntity().getImage()
		);
	}
	
}
