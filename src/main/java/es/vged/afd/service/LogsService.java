package es.vged.afd.service;

import es.vged.afd.data.LogsInput;

public interface LogsService {

	/**
	 * Registers a new LOG
	 * 
	 * @param logsInput Logs Info
	 * @return {@link StandardService}
	 */
	StandardService insertLoginsHistory(LogsInput logsInput);

}