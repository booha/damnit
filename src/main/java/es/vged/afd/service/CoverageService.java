package es.vged.afd.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericInput;
import es.vged.afd.repository.CoverageRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class CoverageService implements CoverageInterface {
	
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject 
	CoverageRepository coverageRepository;
	
	/* (non-Javadoc)
	 * @see es.vged.afd.service.CoverageInterface#getAllCoverages(es.vged.afd.data.GenericInput)
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllCoverages(GenericInput genericInput){
		StandardService response = new StandardService();
		
		List<?> coverages = coverageRepository.findByChannelId(genericInput.getId());
		
		if(coverages.isEmpty()) {
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("No fields found");
			response.setData(coverages);
		}
		
		else {
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Success!");
			response.setData(coverages);
		}
		
		return response;
	}
}
