package es.vged.afd.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericInput;
import es.vged.afd.repository.BenefitRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class BenefitService implements BenefitInterface {
	
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject
	BenefitRepository benefitRepository;
	
	/* (non-Javadoc)
	 * @see es.vged.afd.service.BenefitInterface#getAllBenefits(es.vged.afd.data.GenericInput)
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllBenefits(GenericInput genericInput) {
		StandardService response = new StandardService();
		
		List<?> benefits = benefitRepository.findByChannelId(genericInput.getId());
		
		if(benefits.isEmpty()) {
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("No fields found");
			response.setData(benefits);
		}
		else {
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(benefits);
		}
		
		return response;
	}
}
