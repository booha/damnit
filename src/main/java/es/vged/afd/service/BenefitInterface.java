package es.vged.afd.service;

import es.vged.afd.data.GenericInput;

public interface BenefitInterface {

	StandardService getAllBenefits(GenericInput genericInput);

}