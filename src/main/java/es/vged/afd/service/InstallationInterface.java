package es.vged.afd.service;

import java.util.List;
import com.vged.springarch.responses.GenericResponse;
import es.vged.afd.data.InstallationInput;
import es.vged.afd.data.InstallationOutBean;

public interface InstallationInterface {

	GenericResponse<Boolean> updateInstallation(List<InstallationInput> installationList);

	GenericResponse<Boolean> showMessageUpdateLaborPrice();

	GenericResponse<List<InstallationOutBean>> getAllInstallations();

}