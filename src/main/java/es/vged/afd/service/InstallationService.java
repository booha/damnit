
package es.vged.afd.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.vged.springarch.responses.GenericResponse;
import com.vged.springarch.utils.CommonConstants;

import es.vged.afd.data.InstallationInput;
import es.vged.afd.data.InstallationOutBean;
import es.vged.afd.data.model.InstallationEntity;
import es.vged.afd.data.model.UserEntity;
import es.vged.afd.repository.InstallationRepository;
import es.vged.afd.repository.UserRepository;
import es.vged.afd.rest.security.SecurityHelper;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class InstallationService implements InstallationInterface {

	private static final String AUTH_MODIFY_LABOR_PRICE = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MODIFY_LABOR_PRICE)";
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";
 	private static final String AUTH_MANAGE_USERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_USERS)";
 	
	@Autowired
	InstallationRepository installationRepository;

	@Autowired
	LogsService logsService;

	@Autowired
	UserRepository userRepository;

	@PreAuthorize(value = AUTH_MODIFY_LABOR_PRICE)
	public GenericResponse<Boolean> updateInstallation(List<InstallationInput> installationList) {
		
		String username = SecurityHelper.getPrincipalUsername();
		
		for(InstallationInput installationItem : installationList) {			
			if ( null != installationItem.getInstallationCode() &&  installationItem.getAveragePriceLabor() != null) {
				installationRepository.updateAvgPriceLabor(installationItem.getAveragePriceLabor(), username, new Date(), installationItem.getInstallationCode());
			} 
		}
		
		return handleResponse(Boolean.TRUE); //NOSONAR
	}

	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService listInstallationByDealerCode() {
		StandardService response = new StandardService();

		List<InstallationOutBean> resultList = new ArrayList<>();
		String username = SecurityHelper.getPrincipalUsername();
		UserEntity user = userRepository.findOneByUsername(username);

		List<InstallationEntity> installationList = installationRepository.findByDealerCode(user.getDealerCode());
		for (InstallationEntity installation : installationList) {
			InstallationOutBean installationOutBean = new InstallationOutBean();
			installationOutBean.setBusinessName(installation.getBusinessName());
			installationOutBean.setDealerCode(installation.getDealerCode());
			installationOutBean.setAvgPrice(installation.getAveragePriceLabor());
			installationOutBean.setDescription(installation.getDescription());
			installationOutBean.setInstallationCode(installation.getInstallationCode());
			if (user.getInstallation().getInstallationCode().equals(installation.getInstallationCode())) {
				installationOutBean.setDefaultInstallation(Boolean.TRUE);
			}
			resultList.add(installationOutBean);
		}
		response.setCode(UrlAPIConstants.OK_CODE);
		response.setData(resultList);
		response.setMessage("Success");

		return response;

	}

	@Override
	@PreAuthorize(value = AUTH_MODIFY_LABOR_PRICE)
	public GenericResponse<Boolean> showMessageUpdateLaborPrice() {

		Boolean result = Boolean.FALSE;

		Date currentDate = new Date();
		Calendar calendarCurrentDate = Calendar.getInstance();
		calendarCurrentDate.setTime(currentDate);

		String username = SecurityHelper.getPrincipalUsername();
		UserEntity user = userRepository.findOneByUsername(username);

		List<InstallationEntity> listInstallationsByDealerCode = installationRepository
				.findByDealerCode(user.getDealerCode());

		for (InstallationEntity installationEntity : listInstallationsByDealerCode) {
			
			Calendar calendarInstallationEntity = Calendar.getInstance();
			calendarInstallationEntity.setTime(installationEntity.getLastModifiedDate());

			if (calendarInstallationEntity.get(Calendar.YEAR) < calendarCurrentDate.get(Calendar.YEAR)) {
				result = Boolean.TRUE;
			}
			
		}

		return handleResponse(result);
	}

	private <T> GenericResponse<T> handleResponse(T response) {
		if (response != null) {
			return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.SC_OK), CommonConstants.OK,
					response);
		} else {
			return new GenericResponse<>(CommonConstants.ERROR, String.valueOf(HttpStatus.SC_SERVICE_UNAVAILABLE),
					CommonConstants.SRVC_UNAVAIBLE_MESSAGE, null);
		}
	}

	@Override
	@PreAuthorize(value = AUTH_MANAGE_USERS)
	public GenericResponse<List<InstallationOutBean>> getAllInstallations() {
		List<InstallationEntity> installationList = installationRepository.findAll();
		
		List<InstallationOutBean> listOutput = new ArrayList<>();
		
		for (InstallationEntity installationItem : installationList) {
			InstallationOutBean installationOutput = new InstallationOutBean();
			installationOutput.setInstallationCode(installationItem.getInstallationCode());
			installationOutput.setBusinessName(installationItem.getBusinessName());
			
			listOutput.add(installationOutput);
		}
		
		return handleResponse(listOutput);
	}

}
