package es.vged.afd.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import es.vged.afd.data.GenericOutput;
import es.vged.afd.data.model.ChannelEntity;
import es.vged.afd.repository.ChannelRepository;
import es.vged.afd.utils.UrlAPIConstants;

@Service
public class ChannelService implements ChannelInterface {
	
	private static final Logger logger = LoggerFactory.getLogger(ChannelService.class.getName());
 	private static final String AUTH_MANAGE_OFFERS = "hasAuthority(T(es.vged.afd.rest.security.SecurityConstants).AUTH_MANAGE_OFFERS)";

	@Inject
	ChannelRepository channelRepository;

	/* (non-Javadoc)
	 * @see es.vged.afd.service.ChannelInterface#getAllChannels()
	 */
	@Override
	@PreAuthorize(value = AUTH_MANAGE_OFFERS)
	public StandardService getAllChannels() {

		StandardService response = new StandardService();

		List<GenericOutput> result = new ArrayList<>();
		
		try {

			final List<ChannelEntity> channels = channelRepository.findAll();

			channels.forEach( x -> result.add(converterToGenericOutput(x)) );
			
			response.setCode(UrlAPIConstants.OK_CODE);
			response.setMessage("Succes!");
			response.setData(result);

		} catch (Exception e) { //NOSONAR //TODO:Refactor

			logger.error(e.getMessage());
			
			response.setCode(UrlAPIConstants.KO_CODE);
			response.setMessage("Error");
			response.setData(null);
		}

		return response;
	}

	
	private GenericOutput converterToGenericOutput(ChannelEntity channel) {
		
		return new GenericOutput(
			channel.getId(),
			channel.getName()
		);
	}
}
