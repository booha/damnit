package es.vged.afd.service;

import java.util.List;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.DealerOutput;

public interface DealerInterface {

	GenericResponse<List<DealerOutput>> getAllDealers();

	
}
