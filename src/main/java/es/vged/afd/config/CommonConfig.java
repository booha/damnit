package es.vged.afd.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.vged.springarch")
public class CommonConfig {
    
    
}
