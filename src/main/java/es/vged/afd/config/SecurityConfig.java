package es.vged.afd.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import es.vged.afd.controllers.security.RestAuthenticationEntryPoint;
import es.vged.afd.service.security.JwtAuthenticationFilter;
import es.vged.afd.service.security.TokenUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{

	@Autowired
	private RestAuthenticationEntryPoint unauthorizedHandler;
//
	@Autowired
	@Qualifier("jwtAuthenticationFilter")
	private JwtAuthenticationFilter jwtAuthenticationFilter;

	@Bean
	public UserDetailsService tokenUserDetailsService()
	{
		return new TokenUserDetailsService();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManager()
	{
		return new ProviderManager(Arrays.asList(securityProvider()));
	}

	@Bean
	public UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> userDetailsServiceWrapper()
	{
		return new UserDetailsByNameServiceWrapper<>(tokenUserDetailsService());
	}

	@Bean
	public PreAuthenticatedAuthenticationProvider securityProvider()
	{
		PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
		provider.setPreAuthenticatedUserDetailsService(userDetailsServiceWrapper());
		return provider;
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception // NOPMD
	{
		httpSecurity
				// we don't need CSRF because our token is invulnerable
				.csrf().disable()
				// OPTIONS Calls Allowed
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/version").permitAll()
                .antMatchers("/monitor/*").permitAll()
                .antMatchers("/health").permitAll()
                .antMatchers("/env").permitAll()
                .antMatchers("/env.json").permitAll()
                .antMatchers("/archaius").permitAll()
                .antMatchers("/archaius.json").permitAll()
                .antMatchers("/autoconfig").permitAll()
                .antMatchers("/archaius.json").permitAll()
                .antMatchers("/autoconfig.json").permitAll()
                .antMatchers("/beans").permitAll()
                .antMatchers("/beans.json").permitAll()
                .antMatchers("/configprops").permitAll()
                .antMatchers("/configprops.json").permitAll()
                .antMatchers("/dump").permitAll()
                .antMatchers("/dump.json").permitAll()
                .antMatchers("/info").permitAll()
                .antMatchers("/info.json").permitAll()
                .antMatchers("/mappings").permitAll()
                .antMatchers("/mappings.json").permitAll()
                .antMatchers("/trace").permitAll()
                .antMatchers("/trace.json").permitAll()
                .antMatchers("/error").permitAll()
                .antMatchers("/pause.json").permitAll()
                .antMatchers("/refresh").permitAll()
                .antMatchers("/refresh.json").permitAll()
                .antMatchers("/resume").permitAll()
                .antMatchers("/resume.json").permitAll()
                .antMatchers("/heapdump").permitAll()
                .antMatchers("/heapdump.json").permitAll()
                .antMatchers("/restart").permitAll()
                .antMatchers("/restart.json").permitAll()
                .antMatchers("/metrics").permitAll()
                .antMatchers("/metrics.json").permitAll()
                .antMatchers("/**/*swagger*/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/**/api-docs").permitAll()
                
                .antMatchers("/v1/authentication/**").permitAll()
                
				// All urls must be authenticated (filter for token always fires
				// (/**)
				.anyRequest().authenticated().and()
				// Call our errorHandler if authentication/authorization fails
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
				
				// don't create session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		// Custom JWT based security filter
		httpSecurity.addFilterBefore(jwtAuthenticationFilter, AbstractPreAuthenticatedProcessingFilter.class);

		// disable page caching
		httpSecurity.headers().cacheControl();
	}

}