package es.vged.afd.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "afd-backend.security")
public class SecurityOptions {
	private String ssoURL;
	private String jwtSecret;

	public String getSsoURL() {
		return ssoURL;
	}

	public void setSsoURL(String ssoURL) {
		this.ssoURL = ssoURL;
	}
	
	public String getJwtSecret() {
		return jwtSecret;
	}

	public void setJwtSecret(String jwtSecret) {
		this.jwtSecret = jwtSecret;
	}
}