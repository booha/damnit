package es.vged.afd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import io.fabric8.spring.cloud.kubernetes.archaius.ArchaiusConfigMapSource;

@EnableCircuitBreaker
@SpringBootApplication
@EnableHystrix
@ArchaiusConfigMapSource("afd-backend")
@ImportResource({"classpath:/spring/interceptor-context.xml"})
@ComponentScan
@EnableConfigurationProperties
public class Main {
    public static void main(String...args) {
    	SpringApplication.run(Main.class);
    }
}