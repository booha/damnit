package es.vged.afd.controllers;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.GenericInput;
import es.vged.afd.service.BenefitInterface;
import es.vged.afd.service.StandardService;
import es.vged.afd.utils.UrlAPIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = {
		@ApiResponse(code = 200, response = GenericResponse.class, message = "Success"),
		@ApiResponse(code = 500, response = GenericResponse.class, message = "Internal Server Error"),
		@ApiResponse(code = 400, response = GenericResponse.class, message = "Bad Request") })
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(UrlAPIConstants.API_VERSION + UrlAPIConstants.BENEFITS_RESOURCE)
public class BenefitsEndpoint {
	
	@Inject
	BenefitInterface benefitService;
	
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get all benefit by channel")
	@RequestMapping(value = UrlAPIConstants.ID_VAR, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService getBenefitByChannel(
			@PathVariable("id") Integer channelId
			) {
	
		GenericInput genericInput = new GenericInput(channelId, "");

		return benefitService.getAllBenefits(genericInput);
	}
	
}
