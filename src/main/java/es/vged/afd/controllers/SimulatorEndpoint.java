package es.vged.afd.controllers;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.SimulatorInput;
import es.vged.afd.service.SimulatorInterface;
import es.vged.afd.service.StandardService;
import es.vged.afd.utils.UrlAPIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = { @ApiResponse(code = 200, response = GenericResponse.class, message = "Success"),
		@ApiResponse(code = 500, response = GenericResponse.class, message = "Internal Server Error"),
		@ApiResponse(code = 400, response = GenericResponse.class, message = "Bad Request") })
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE)
public class SimulatorEndpoint
{

	@Inject
	SimulatorInterface simulatorService;

	// ex.: "/v1/simulator/{model}/{contract}/{km}"
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get Simulator Calculations")
	@RequestMapping(value = UrlAPIConstants.MODEL_VAR + UrlAPIConstants.CONTRACT_VAR
			+ UrlAPIConstants.KM_VAR, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Deprecated
	public StandardService getSimulatorCalculations(@PathVariable("model") Integer modelId,
			@PathVariable("contract") Integer contractId, @PathVariable("km") Integer kmId)
	{

		SimulatorInput simulatorInput = new SimulatorInput(modelId, kmId, contractId);

		return simulatorService.simCalculs(simulatorInput);

	}

	// ex.: "/v1/simulator/{model}/{contract}/{km}/{installationCode}"
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get Simulator Calculations")
	@RequestMapping(value = UrlAPIConstants.MODEL_VAR + UrlAPIConstants.CONTRACT_VAR + UrlAPIConstants.KM_VAR
			+ UrlAPIConstants.INSTALLATION_CODE_VAR, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService getSimulatorCalculations(@PathVariable("model") Integer modelId,
			@PathVariable("contract") Integer contractId, @PathVariable("km") Integer kmId,
			@PathVariable("installationCode") String installationCode)
	{

		SimulatorInput simulatorInput = new SimulatorInput(modelId, kmId, contractId);

		return simulatorService.simCalculs(simulatorInput, installationCode);

	}

}
