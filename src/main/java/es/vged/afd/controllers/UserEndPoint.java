package es.vged.afd.controllers;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.UserBean;
import es.vged.afd.data.UserInput;
import es.vged.afd.data.UserOutputBean;
import es.vged.afd.service.LogsService;
import es.vged.afd.service.StandardService;
import es.vged.afd.service.UserInterface;
import es.vged.afd.service.UserService;
import es.vged.afd.utils.UrlAPIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = {
		@ApiResponse(code = 200, response = GenericResponse.class, message = "Success"),
		@ApiResponse(code = 500, response = GenericResponse.class, message = "Internal Server Error"),
		@ApiResponse(code = 400, response = GenericResponse.class, message = "Bad Request") })
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE)
public class UserEndPoint {
	
	@Inject
	UserService userService;
	
	@Inject
	LogsService logsService;
	
	@Autowired
	private UserInterface userInterface;
	
	//ex.: "/v1/user/{username}" 
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get User data and his installation")
	@RequestMapping(value =  UrlAPIConstants.USERNAME_VAR, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService getUserInstallation(@PathVariable("username") String username) {
		
		return userService.getUserInstallation(username);

	}
	
	
	/*ex.: "/v1/user/lastdateaccess/"
	 * Body:"username" : "{username}"
	*/
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update user last date access ")
	@RequestMapping(value =  UrlAPIConstants.USER_LAST_DATE_ACCESS_RESOURCE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService updateLastDateAccess(
			@RequestBody UserInput userInput
			) {
		
		return userService.updateLastDateAccess(userInput);

	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Check User Installation")
	@RequestMapping(value = UrlAPIConstants.USER_INSTALLATION, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService checkUserInstallation() {
		
		return userService.checkUserInstallation();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Create new user")
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<Boolean> createUser(@RequestBody  UserBean userBean) {
		
		return userInterface.createAndUpdateUser(userBean);
		
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get user by filter")
	@RequestMapping(value = UrlAPIConstants.ALL + UrlAPIConstants.NAME, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<List<UserOutputBean>> getUsers(@PathVariable("name") String name) {
		
		return userInterface.getUsers(name);
		
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete user")
	@RequestMapping(value = UrlAPIConstants.ID_VAR, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<Boolean> deleteUser(@PathVariable("id") Integer id) {
		
		return userInterface.deleteUser(id);
		
	}

}
