package es.vged.afd.controllers.security;

public class HeaderConstants {
	public static final String SSO_TOKEN = "SSO-JWT";
	public static final String USER_TOKEN = "USER-JWT";
	
	private HeaderConstants() {}
}