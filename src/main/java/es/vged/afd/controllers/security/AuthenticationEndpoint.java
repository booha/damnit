 package es.vged.afd.controllers.security;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.vged.afd.common.audit.LoggerHelper;
import es.vged.afd.service.StandardService;
import es.vged.afd.service.security.AuthenticationService;
import es.vged.afd.service.security.JwtAuthenticationFilter;
import es.vged.afd.utils.UrlAPIConstants;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(UrlAPIConstants.API_VERSION + UrlAPIConstants.AUTHENTICATION_ROOT)
public class AuthenticationEndpoint
{
	private static final LoggerHelper LOGGER = LoggerHelper.getLoggerHelper(JwtAuthenticationFilter.class);

	@Inject
	private AuthenticationService authenticationService;
	
	

	@RequestMapping(value = UrlAPIConstants.AUTHENTICATION_CREATE_TOKEN, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public StandardService createToken(@RequestHeader(HeaderConstants.SSO_TOKEN) String ssoToken) 
	{
		LOGGER.info("Call with SSO-TOKEN:" + ssoToken);

		StandardService response = new StandardService();
		response.setCode(200);
		response.setMessage(authenticationService.createToken(ssoToken));
		return response;
	}
}