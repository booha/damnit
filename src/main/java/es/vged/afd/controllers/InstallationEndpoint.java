package es.vged.afd.controllers;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vged.springarch.responses.GenericResponse;

import es.vged.afd.data.InstallationInput;
import es.vged.afd.data.InstallationOutBean;
import es.vged.afd.service.InstallationInterface;
import es.vged.afd.service.InstallationService;
import es.vged.afd.service.StandardService;
import es.vged.afd.utils.UrlAPIConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@ApiResponses(value = {
		@ApiResponse(code = 200, response = GenericResponse.class, message = "Success"),
		@ApiResponse(code = 500, response = GenericResponse.class, message = "Internal Server Error"),
		@ApiResponse(code = 400, response = GenericResponse.class, message = "Bad Request") })
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(UrlAPIConstants.API_VERSION + UrlAPIConstants.INSTALLATION_RESOURCE)
public class InstallationEndpoint {

	private static final Logger logger = LoggerFactory.getLogger(InstallationEndpoint.class.getName());

	@Inject
	InstallationService installationService;
	
	@Autowired
	InstallationInterface installationInterface;
	
	/*ex.: "/v1/installation/"
	 * Body:
	 * "id" : "{id}",
	 * "name" : "{name}",
	 * "averagePriceLabor" : "{averagePriceLabor}",
	*/
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update installation")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<Boolean> updateInstallation(
			@ApiParam(value = "installationInput", required = true) @RequestBody List<InstallationInput> installationList) {

		return installationInterface.updateInstallation(installationList);
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "List installation by User")
	@RequestMapping(method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public StandardService findInstallationByUser() {
		return installationService.listInstallationByDealerCode();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Show messages to update labor price")
	@RequestMapping(value = UrlAPIConstants.INSTALLATION_PRICE_MESSAGE, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<Boolean> showMessageUpdateLaborPrice() {
		logger.debug("Executing GET message labor price");
		return installationInterface.showMessageUpdateLaborPrice();
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "List installation")
	@RequestMapping(value = UrlAPIConstants.ALL, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse<List<InstallationOutBean>> getAllInstallations() {
		return installationInterface.getAllInstallations();
	}
	
}
