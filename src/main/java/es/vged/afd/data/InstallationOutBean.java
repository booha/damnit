package es.vged.afd.data;

import java.io.Serializable;

public class InstallationOutBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String businessName;
	private String description;
	private Double avgPrice;
	private String installationCode;
	private String dealerCode;
	private Boolean defaultInstallation;

	public InstallationOutBean() {
		super();
	}

	public InstallationOutBean(String installationCode, String businessName, Double averagePriceLabor) {
		super();
		this.installationCode = installationCode;
		this.businessName = businessName;
		this.avgPrice = averagePriceLabor;
	}

	public Boolean getDefaultInstallation() {
		return defaultInstallation;
	}

	public Double getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(Double avgPrice) {
		this.avgPrice = avgPrice;
	}

	public void setDefaultInstallation(Boolean defaultInstallation) {
		this.defaultInstallation = defaultInstallation;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	@Override
	public String toString() {
		return "InstallationOutBean [businessName=" + businessName + ", description=" + description
				+ ", installationCode=" + installationCode + ", dealerCode=" + dealerCode + "]";
	}

}
