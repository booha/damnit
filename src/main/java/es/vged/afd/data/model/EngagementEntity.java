package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ENGAGEMENT")
public class EngagementEntity extends AuditableEntity { //NOSONAR //TODO:Refactor

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", nullable = false)
	private ChannelEntity channel;
	
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="ENGAGEMENT_ID")
	private List<PlanEngagementEntity> planEngagements; //NOSONAR //TODO:Refactor
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="ENGAGEMENT_ID")
	private List<AnnualkmEngagementEntity> annualkmEngagements; //NOSONAR //TODO:Refactor
	
	
	public EngagementEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public EngagementEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public EngagementEntity(int id, String name, String description, ChannelEntity channel) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.channel = channel;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ChannelEntity getChannel() {
		return channel;
	}

	public void setChannel(ChannelEntity channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "EngagementEntity [id=" + id + ", name=" + name + ", description=" + description 
				+ ", channel=" + channel + "]";
	}

}
