package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MODEL_TYPE")
public class ModelTypeEntity extends AuditableEntity { //NOSONAR //TODO:Refactor

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@Column(name = "IMAGE", nullable = true)
	private String image;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="MODEL_TYPE_ID")
	private List<ModelEntity> models; //NOSONAR //TODO:Refactor
	
	public ModelTypeEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public ModelTypeEntity(Integer id, String name, String description, String image) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.image = image;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ModelTypeEntity [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
