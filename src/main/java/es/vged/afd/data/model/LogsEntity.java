package es.vged.afd.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOGS")
public class LogsEntity extends AuditableEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@Id
	@SequenceGenerator(name = "LOGS_SEQ", sequenceName = "LOGS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOGS_SEQ")
	private Integer id;

	@Column(name = "USER_ID", precision = 4)
	private Integer idUser;

	@Column(name = "INSTALLATION_CODE", length = 5)
	private String idInstallation;

	@Column(name = "LOG_TYPE", precision = 3)
	private Integer logType;

	@Column(name = "VALUE", precision = 11, scale = 2)
	private Double value;

	public LogsEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer integer) {
		this.id = integer;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getIdInstallation() {
		return idInstallation;
	}

	public void setIdInstallation(String idInstallation) {
		this.idInstallation = idInstallation;
	}

	public Integer getLogType() {
		return logType;
	}

	public void setLogType(Integer logType) {
		this.logType = logType;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "LogsEntity [id=" + id + ", idUser=" + idUser + ", idInstallation=" + idInstallation + ", logType="
				+ logType + ", value=" + value + "]";
	}

}