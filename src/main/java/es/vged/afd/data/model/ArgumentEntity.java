package es.vged.afd.data.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ARGUMENT")
public class ArgumentEntity extends AuditableEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "TEXT")
	private String text;
	
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", nullable = false)
	private ChannelEntity channel;

	public ArgumentEntity() { //NOSONAR //TODO:Refactor
		super();
	}
	public ArgumentEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public ArgumentEntity(int id, String title, String text, ChannelEntity channel) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.channel = channel;
	}

	
	public void setId(Integer id) {
		this.id = id;
	}


	public String getTitle() { 
		return title; 
	}

	public void setTitle(String title) { 
		this.title = title; 
	}

	public String getText() { 
		return text; 
	}

	public void setText(String text) { 
		this.text = text; 
	}
	
	public ChannelEntity getChannel() {
		return channel;
	}
	public void setChannel(ChannelEntity channel) {
		this.channel = channel;
	}
	@Override
	public String toString() {
		return "ArgumentEntity [id=" + id + ", title=" + title + ", text=" + text + ", channel=" + channel + "]";
	}
	
	
	

}
