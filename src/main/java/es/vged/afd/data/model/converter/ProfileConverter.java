package es.vged.afd.data.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import es.vged.afd.data.model.enumeration.ProfileEnum;

/**
 * Attribute converter to allow JPA to transform the DB value to the corresponding business enumeration
 * {@link DataScopeTypeEnum} and vice versa.
 *
 * @author juan.garcia
 */
@Converter
public class ProfileConverter implements AttributeConverter<ProfileEnum, Integer>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer convertToDatabaseColumn(ProfileEnum attribute)
	{
		Integer result = null;
		if (attribute != null)
		{
			result = attribute.getValue();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProfileEnum convertToEntityAttribute(Integer dbData)
	{
		ProfileEnum result = null;
		if (dbData != null)
		{
			result = ProfileEnum.fromValue(dbData);
		}
		return result;
	}

}
