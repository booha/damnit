package es.vged.afd.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@MappedSuperclass
public class AuditableEntity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "CREATED_DATE", updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

//	@Version
	@Column(name = "MODIFIED_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@Column(name = "CREATED_BY", updatable = false, nullable = false, length = 50)
	private String createdBy;

	@Column(name = "MODIFIED_BY", nullable = false, length = 50)
	private String lastModifiedBy;

	/*
	 * Default Constructor
	 */
	public AuditableEntity() // NOPMD
	{
	}

	public AuditableEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy)
	{
		this.createdBy = createdBy;
		this.createdDate = createdDate; //NOSONAR
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedDate = lastModifiedDate; //NOSONAR
	}

	public Date getCreatedDate()
	{
		return createdDate; //NOSONAR
	}

	public void setCreatedDate(Date createdDate)
	{
		if (this.createdDate == null)
		{
			this.createdDate = createdDate; //NOSONAR
		}
	}

	public Date getLastModifiedDate()
	{
		return lastModifiedDate; //NOSONAR
	}

	public void setLastModifiedDate(Date lastModifiedDate)
	{
		this.lastModifiedDate = lastModifiedDate; //NOSONAR
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		if (this.createdBy == null)
		{
			this.createdBy = createdBy;
		}
	}

	public String getLastModifiedBy()
	{
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy)
	{
		this.lastModifiedBy = lastModifiedBy;
	}

	@PrePersist
	protected void setDefaultsOnCreate()
	{
		lastModifiedBy = es.vged.afd.rest.security.SecurityHelper.getPrincipalUsername();
		createdBy = lastModifiedBy;
		Date currentDate = new Date();
		createdDate = currentDate;
		lastModifiedDate = currentDate;

	}

	@PreUpdate
	protected void setDefaultsOnUpdate()
	{
		lastModifiedBy = es.vged.afd.rest.security.SecurityHelper.getPrincipalUsername();
		Date currentDate = new Date();
		lastModifiedDate = currentDate;
	}



}