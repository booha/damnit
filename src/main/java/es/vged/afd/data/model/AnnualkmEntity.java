package es.vged.afd.data.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "ANNUALKM")
public class AnnualkmEntity extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="ANNUALKM_ID")
	private List<SimulatorEntity> simulators;

	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="ANNUALKM_ID")
	private List<AnnualkmEngagementEntity> annualkmEngagements;

	
	public AnnualkmEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public AnnualkmEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public AnnualkmEntity(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AnnualkmEntity [id=" + id + ", name=" + name + ", description=" + description + ", simulators="
				+ simulators + ", annualkmEngagements=" + annualkmEngagements + "]";
	}

}
