package es.vged.afd.data.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/*Entidad TEMPORAL hasta la definición y realización del SSO
 * 
 * */

@Entity
@Table(name = "AFDUSER")
public class UserEntity extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "USERNAME", nullable = false)
	private String username;

	@Column(name = "USER_COMPLETENAME", nullable = true)
	private String userCompleteName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_ACCESS_DATE", nullable = true)
	private Date lastDateAccess;

	@ManyToOne
	@JoinColumn(name = "DEFAULT_INSTALLATION_CODE", nullable = false)
	private InstallationEntity installation;

	@Column(name = "DEALER_CODE", nullable = true)
	private String dealerCode;
	
	@ManyToOne
 	@JoinColumn(name = "DEALER_ID")
 	private DealerEntity dealer;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.user", orphanRemoval = true)
	private List<UserProfileEntity> userProfiles;


	public UserEntity() { // NOSONAR //TODO:Refactor
		super();
	}

	public UserEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { // NOSONAR
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public UserEntity(Integer id, String username, String userCompleteName, Date lastDateAccess,
			InstallationEntity installation, String dealerCode, DealerEntity dealer) {
		super();
		this.id = id;
		this.username = username;
		this.userCompleteName = userCompleteName;
		this.lastDateAccess = lastDateAccess;
		this.installation = installation;
		this.dealerCode = dealerCode;
		this.dealer = dealer;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserCompleteName() {
		return userCompleteName;
	}

	public void setUserCompleteName(String userCompleteName) {
		this.userCompleteName = userCompleteName;
	}

	public Date getLastDateAccess() {
		return lastDateAccess;
	}

	public void setLastDateAccess(Date lastDateAccess) {
		this.lastDateAccess = lastDateAccess;
	}

	public InstallationEntity getInstallation() {
		return installation;
	}

	public void setInstallation(InstallationEntity installation) {
		this.installation = installation;
	}

	public DealerEntity getDealer() {
		return dealer;
	}

	public void setDealer(DealerEntity dealer) {
		this.dealer = dealer;
	}

	

	public List<UserProfileEntity> getUserProfile() {
		return userProfiles;
	}

	public void setUserProfile(List<UserProfileEntity> userProfile) {
		this.userProfiles = userProfile;
	}
	

	public List<ProfileEntity> getProfilesList()
	{
		List<ProfileEntity> profileList = new ArrayList<>();
		if (this.userProfiles != null)
		{
			for (UserProfileEntity up : this.userProfiles)
			{
				profileList.add(up.getProfileEntity());
			}
		}
		return profileList;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", username=" + username + ", userCompleteName=" + userCompleteName
				+ ", lastDateAccess=" + lastDateAccess + ", installation=" + installation + ", dealerCode=" + dealerCode
				+ ", dealer=" + dealer + "]";
	}
}
