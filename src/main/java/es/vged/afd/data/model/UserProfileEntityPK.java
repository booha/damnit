package es.vged.afd.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class UserProfileEntityPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private UserEntity user;

	@ManyToOne
	@JoinColumn(name = "PROFILE_ID", nullable = false)
	private ProfileEntity profile;

	public UserProfileEntityPK() {
		super();
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity userEntity) {
		this.user = userEntity;
	}

	public ProfileEntity getProfile() {
		return profile;
	}

	public void setProfile(ProfileEntity profileEntity) {
		this.profile = profileEntity;
	}

}
