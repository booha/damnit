package es.vged.afd.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

import javax.persistence.Column;

@Entity
@Table(name = "SIMULATOR")
public class SimulatorEntity extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "FEE_AFD", nullable = true, precision = 10, scale = 2)
	private Double feeAfd;
	
	@Column(name = "LONG_COST", nullable = true, precision = 10, scale = 2)
	private Double longCost;
	
	@Column(name = "SHORT_COST", nullable = true, precision = 10, scale = 2)
	private Double shortCost;
	
	@Column(name = "MONTH_CAR", nullable = true)
	private Integer monthCar;

	@Column(name = "LABOR_PERCENTAGE", nullable = true, precision = 10, scale = 2)
	private Double laborPercentage;
	
	@ManyToOne
	@JoinColumn(name = "PLANENGAGEMENT_ID", nullable = false)
	private PlanEngagementEntity planengagement;
	
	@ManyToOne
	@JoinColumn(name = "MODEL_ID", nullable = false)
	private ModelEntity model;
	
	@ManyToOne
	@JoinColumn(name = "ANNUALKM_ID", nullable = false)
	private AnnualkmEntity annualkm;
	
	public SimulatorEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public SimulatorEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getFeeAfd() {
		return feeAfd;
	}

	public void setFeeAfd(Double feeAfd) {
		this.feeAfd = feeAfd;
	}

	public Double getLongCost() {
		return longCost;
	}

	public void setLongCost(Double longCost) {
		this.longCost = longCost;
	}

	public Double getShortCost() {
		return shortCost;
	}

	public void setShortCost(Double shortCost) {
		this.shortCost = shortCost;
	}

	public Integer getMonthCar() {
		return monthCar;
	}

	public void setMonthCar(Integer monthCar) {
		this.monthCar = monthCar;
	}

	public Double getLaborPercentage() {
		return laborPercentage;
	}

	public void setLaborPercentage(Double laborPercentage) {
		this.laborPercentage = laborPercentage;
	}

	public PlanEngagementEntity getPlanengagement() {
		return planengagement;
	}

	public void setPlanengagement(PlanEngagementEntity planengagement) {
		this.planengagement = planengagement;
	}

	public ModelEntity getModel() {
		return model;
	}

	public void setModel(ModelEntity model) {
		this.model = model;
	}

	public AnnualkmEntity getAnnualkm() {
		return annualkm;
	}

	public void setAnnualkm(AnnualkmEntity annualkm) {
		this.annualkm = annualkm;
	}

	@Override
	public String toString() {
		return "SimulatorEntity [id=" + id + ", feeAfd = " + feeAfd + ", longCost = " + longCost + ", shortCost = " + shortCost + 
				", monthCar =" + monthCar + ", laborPercentage =" + laborPercentage + ", planEngagement = " + planengagement + 
				", annualkm = " + annualkm + ", model = " + model + "]";
	}

}
