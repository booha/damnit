package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "INSTALLATION")
public class InstallationEntity extends AuditableEntity { //NOSONAR //TODO:Refactor

	@Id
	@Column(name = "INSTALLATION_CODE", unique = true, nullable = false, length = 5)
	private String installationCode;
	
	@Column(name = "BUSINESS_NAME", nullable = false, length = 100)
	private String businessName;
	
	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@Column(name = "AVG_PRICE_LABOR", nullable = false, precision = 10, scale = 2)
	private Double averagePriceLabor;	
	
	@Column(name= "DEALER_CODE", nullable = true)
 	private String dealerCode;
	
	@ManyToOne
	@JoinColumn(name= "DEALER_ID")
	private DealerEntity dealer;
	
	public InstallationEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public InstallationEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public InstallationEntity(String installationCode, String businessName, String description, Double averagePriceLabor) {
		super();
		this.installationCode = installationCode;
		this.businessName = businessName;
		this.description = description;
		this.averagePriceLabor = averagePriceLabor;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}


	public DealerEntity getDealer() {
		return dealer;
	}

	public void setDealer(DealerEntity dealer) {
		this.dealer = dealer;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAveragePriceLabor() {
		return averagePriceLabor;
	}

	public void setAveragePriceLabor(Double averagePriceLabor) {
		this.averagePriceLabor = averagePriceLabor;
	}
	
	public String getDealerCode() {
 		return dealerCode;
	}

	public void setDealerCode(String dealerCode) {
 		this.dealerCode = dealerCode;
	}

	@Override
	public String toString() {
		return "InstallationEntity [installationCode=" + installationCode + ", businessName=" + businessName
				+ ", description=" + description + ", averagePriceLabor=" + averagePriceLabor + ", dealerCode="
				+ dealerCode + ", dealer=" + dealer + "]";
	}
	
}
