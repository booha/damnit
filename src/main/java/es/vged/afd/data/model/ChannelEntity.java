package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "CHANNEL")
public class ChannelEntity extends AuditableEntity { //NOSONAR //TODO:Refactor

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="CHANNEL_ID")
	private List<EngagementEntity> engagements; //NOSONAR //TODO:Refactor

	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="CHANNEL_ID")
	private List<CoverageEntity> coverages; //NOSONAR //TODO:Refactor
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="CHANNEL_ID")
	private List<ArgumentEntity> arguments; //NOSONAR //TODO:Refactor
	

	public ChannelEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public ChannelEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public ChannelEntity(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "ChannelEntity [id=" + id + ", name=" + name + "]";
	}
}