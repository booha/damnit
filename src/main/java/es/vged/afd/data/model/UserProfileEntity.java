package es.vged.afd.data.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "USER_PROFILE")
@NamedQuery(name = "UserProfileEntity.findAll", query = "SELECT u FROM UserProfileEntity u")
@AssociationOverrides({ @AssociationOverride(name = "pk.profile", joinColumns = @JoinColumn(name = "PROFILE_ID")),
		@AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "USER_ID")) })
public class UserProfileEntity extends AuditableEntity {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private UserProfileEntityPK pk = new UserProfileEntityPK();

	public UserProfileEntityPK getPk() {
		return pk;
	}

	public void setPk(UserProfileEntityPK pk) {
		this.pk = pk;
	}

	public UserProfileEntity(UserProfileEntityPK pk) {
		super();
		this.pk = pk;
	}

	public UserProfileEntity() {
		super();
	}

	@Transient
	public UserEntity getUserEntity() {
		return pk.getUser();
	}

	public void setUserEntity(UserEntity userEntity) {
		this.pk.setUser(userEntity);
	}

	@Transient
	public ProfileEntity getProfileEntity() {
		return pk.getProfile();
	}

	public void setProfileEntity(ProfileEntity profileEntity) {
		this.pk.setProfile(profileEntity);
	}

}