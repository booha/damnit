package es.vged.afd.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class ProfileAuthorityEntityPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "PROFILE_ID", nullable = false)
	private ProfileEntity profile;

	@ManyToOne
	@JoinColumn(name = "AUTHORITY_ID", nullable = false)
	private AuthorityEntity authority;

	public ProfileAuthorityEntityPK() {
		super();
	}

	public ProfileEntity getProfile() {
		return profile;
	}

	public void setProfile(ProfileEntity profile) {
		this.profile = profile;
	}

	public AuthorityEntity getAuthority() {
		return authority;
	}

	public void setAuthority(AuthorityEntity authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "ProfileAuthorityEntityPK [profile=" + profile + ", authority=" + authority + "]";
	}
}
