package es.vged.afd.data.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PROFILE_AUTHORITY")
@NamedQuery(name = "ProfileAuthorityEntity.findAll", query = "SELECT pa FROM ProfileAuthorityEntity pa")
@AssociationOverrides({ @AssociationOverride(name = "pk.profile", joinColumns = @JoinColumn(name = "PROFILE_ID")),
		@AssociationOverride(name = "pk.authority", joinColumns = @JoinColumn(name = "AUTHORITY_ID")) })
public class ProfileAuthorityEntity extends AuditableEntity{

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProfileAuthorityEntityPK pk = new ProfileAuthorityEntityPK();

	public ProfileAuthorityEntityPK getPk() {
		return pk;
	}

	public void setPk(ProfileAuthorityEntityPK pk) {
		this.pk = pk;
	}

	@Transient
	public AuthorityEntity getAuthority() {
		return pk.getAuthority();
	}

	public void setAuthority(AuthorityEntity authority) {
		this.pk.setAuthority(authority);
	}

	@Transient
	public ProfileEntity getProfile() {
		return pk.getProfile();
	}

	public void setProfile(ProfileEntity profile) {
		this.pk.setProfile(profile);
	}
	
}
