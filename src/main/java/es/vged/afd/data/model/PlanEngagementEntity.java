package es.vged.afd.data.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "PLAN_ENGAGEMENT")
public class PlanEngagementEntity extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "MONTH_PROMOTION", nullable = true)
	private Integer monthPromotion;
	
	@Column(name = "COPAYMENT", nullable = true, precision = 10, scale = 2)
	private Double copayment;

	@ManyToOne
	@JoinColumn(name = "ENGAGEMENT_ID", nullable = false)
	private EngagementEntity engagement;
	
	@ManyToOne
	@JoinColumn(name = "PLAN_ID", nullable = false)
	private PlanEntity plan;
	
	public PlanEngagementEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public PlanEngagementEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public PlanEngagementEntity(int id, Integer monthPromotion, Double copayment, EngagementEntity engagement, PlanEntity plan) {
		super();
		this.id = id;
		this.monthPromotion = monthPromotion;
		this.copayment = copayment;
		this.engagement = engagement;
		this.plan = plan;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMonthpromotion() {
		return monthPromotion;
	}

	public void setMonthpromotion(Integer monthpromotion) {
		this.monthPromotion = monthpromotion;
	}

	public Double getCopayment() {
		return copayment;
	}

	public void setCopayment(Double copayment) {
		this.copayment = copayment;
	}

	public EngagementEntity getEngagement() {
		return engagement;
	}

	public void setEngagement(EngagementEntity engagement) {
		this.engagement = engagement;
	}

	public PlanEntity getPlan() {
		return plan;
	}

	public void setPlan(PlanEntity plan) {
		this.plan = plan;
	}

	@Override
	public String toString() {
		return "PlanEngagementEntity [id=" + id + ", monthpromotion = " + monthPromotion + ", copago = " + copayment + ", plan = " + plan + ", engagement =" + engagement + "]";
	}
	
}
