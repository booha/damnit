package es.vged.afd.data.model;

import java.util.List;

import javax.jdo.annotations.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AUTHORITY")
public class AuthorityEntity extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME")
	private String name;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.authority", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProfileAuthorityEntity> profileAuthorities;

	public AuthorityEntity() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProfileAuthorityEntity> getProfileAuthorities() {
		return profileAuthorities;
	}

	public void setProfileAuthorities(List<ProfileAuthorityEntity> profileAuthorities) {
		this.profileAuthorities = profileAuthorities;
	}

	@Override
	public String toString() {
		return "AuthorityEntity [id=" + id + ", name=" + name + "]";
	}
	
}
