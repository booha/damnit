package es.vged.afd.data.model.enumeration;

import java.util.concurrent.ConcurrentHashMap;


public enum ProfileEnum {
	
	DEALER (1),
	
	DEALERMO(2),
	
	HELPDESK(3),
	
	ADMIN(4);
	
	private static final ConcurrentHashMap<Integer, ProfileEnum> VALUES = new ConcurrentHashMap<>();

	static
	{
		for (ProfileEnum status : ProfileEnum.values())
		{
			VALUES.put(status.value, status);
		}
	}

	private int value;

	ProfileEnum(int value)
	{
		this.value = value;
	}

	/**
	 * Resolves a {@link GoalModelGroupStatusEnum} from its code
	 * 
	 * @param value
	 *            code to look for
	 * @return GoalModelGroupStatusEnum
	 * @throws IllegalArgumentException
	 *             if the given code does not correspond to any of the action
	 */
	public static ProfileEnum fromValue(int value)
	{
		ProfileEnum type = VALUES.get(value);
		if (type != null)
		{
			return type;
		}

		throw new IllegalArgumentException("There isn't any  action with the code '" + value + "'.");
	}

	/**
	 * @return the value that represents the type
	 */
	public int getValue()
	{
		return value;
	}
	
}
