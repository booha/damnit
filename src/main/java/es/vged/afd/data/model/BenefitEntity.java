package es.vged.afd.data.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BENEFIT")
public class BenefitEntity extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "ENTRY", nullable = false)
	private boolean entry;

	@Column(name = "PLUS", nullable = false)
	private boolean plus;

	@Column(name = "TEXT", nullable = false)
	private String text;

	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", nullable = false)
	private ChannelEntity channel;

	public BenefitEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public BenefitEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public BenefitEntity(Integer id, boolean entry, boolean plus, String text, ChannelEntity channel) {
		super();
		this.id = id;
		this.entry = entry;
		this.plus = plus;
		this.text = text;
		this.channel = channel;
	}

	public int getId() { return id; }

	public void setId(Integer id) { this.id = id; }

	public boolean isEntry() { return entry; }

	public void setEntry(boolean entry) { this.entry = entry; }

	public boolean isPlus() { return plus; }

	public void setPlus(boolean plus) { this.plus = plus; }

	public String getText() { return text; }

	public void setText(String text) { this.text = text; }

	public ChannelEntity getChannel() {
		return channel;
	}

	public void setChannel(ChannelEntity channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "BenefitEntity [id=" + id + ", entry=" + entry + ", plus=" + plus + ", text=" + text + ", channel="
				+ channel + "]";
	}
}
