package es.vged.afd.data.model.enumeration;

public enum AnnualKmEngagementFormulaEnum {

	SAVINGS_VN_NOFINANCIAL,
	SAVINGS_VN_FINANCIAL,
	SAVINGS_VO_NOFINANCIAL,
	SAVINGS_VO_FINANCIAL,
	SAVINGS_PV

}
