package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AFDPLAN")
public class PlanEntity extends AuditableEntity{ //NOSONAR //TODO:Refactor

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@Column(name = "ORDER_PLAN", nullable = false)
	private Integer order;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="PLAN_ID")
	private List<PlanEngagementEntity> planEngagements; //NOSONAR //TODO:Refactor

	
	public PlanEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public PlanEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public PlanEntity(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PlanEntity [id=" + id + ", name=" + name + ", description=" + description + ", order=" + order + "]";
	}

}
