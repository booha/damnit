package es.vged.afd.data.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COVERAGE")
public class CoverageEntity extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "TEXT")
	private String text;
	
	@Column(name = "ENTRY")
	private boolean entry;
	
	@Column(name = "PLUS")
	private boolean plus;
	
	@Column(name = "LINK")
	private String link;
	
	@ManyToOne
	@JoinColumn(name = "CHANNEL_ID", nullable = false)
	private ChannelEntity channel;

	public CoverageEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public CoverageEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public CoverageEntity(Integer id, String text, boolean entry, boolean plus, String link, ChannelEntity channel) {
		super();
		this.id = id;
		this.text = text;
		this.entry = entry;
		this.plus = plus;
		this.link = link;
		this.channel = channel;
	}

	public Integer getId() { return id; }

	public void setId(Integer id) { this.id = id; }

	public String getText() { return text; }

	public void setText(String text) { this.text = text; }

	public boolean isEntry() { return entry; }

	public void setEntry(boolean entry) { this.entry = entry; }

	public boolean isPlus() { return plus; }

	public void setPlus(boolean plus) { this.plus = plus; }

	public String getLink() { return link; }

	public void setLink(String link) { this.link = link; }

	public ChannelEntity getChannel() {
		return channel;
	}

	public void setChannel(ChannelEntity channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "CoverageEntity [id=" + id + ", text=" + text + ", entry=" + entry + ", plus=" + plus + ", link=" + link
				+ ", channel=" + channel + "]";
	}
}
