package es.vged.afd.data.model; //NOSONAR //TODO:Refactor

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "MODEL")
public class ModelEntity extends AuditableEntity { //NOSONAR //TODO:Refactor
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@ManyToOne
	@JoinColumn(name = "MODEL_TYPE_ID", nullable = true)
	private ModelTypeEntity modelTypeEntity;
	
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="MODEL_ID")
	private List<SimulatorEntity> simulators; //NOSONAR //TODO:Refactor

	
	public ModelEntity() { //NOSONAR //TODO:Refactor
		super();
	}

	public ModelEntity(Date createdDate, Date lastModifiedDate, String createdBy, String lastModifiedBy) { //NOSONAR //TODO:Refactor
		super(createdDate, lastModifiedDate, createdBy, lastModifiedBy);
	}

	public ModelEntity(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public ModelTypeEntity getModelTypeEntity() {
		return modelTypeEntity;
	}

	public void setModelTypeEntity(ModelTypeEntity modelTypeEntity) {
		this.modelTypeEntity = modelTypeEntity;
	}

	@Override
	public String toString() {
		return "ModelEntity [id=" + id + ", name=" + name + "]";
	}
	
}
