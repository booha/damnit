package es.vged.afd.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ANNUALKM_ENGAGEMENT")
public class AnnualkmEngagementEntity extends AuditableEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "START_PAYMENT_MONTHLY", nullable = true)
	private Integer startPaymentMonthly;

	@Column(name = "CAR_AGE", nullable = true)
	private Integer carAge;

	@Column(name = "VAR_SHORT_TERM_MONTHS", nullable = false)
	private Integer shortTermVar;

	@Column(name = "VAR_LONG_TERM_MONTHS", nullable = false)
	private Integer longTermVar;

	@Column(name = "FORMULA", nullable = false)
	private String formula;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@ManyToOne
	@JoinColumn(name = "ANNUALKM_ID", nullable = false)
	private AnnualkmEntity annualkm;

	@ManyToOne
	@JoinColumn(name = "ENGAGEMENT_ID", nullable = false)
	private EngagementEntity engagement;

	public AnnualkmEngagementEntity() { // NOSONAR //TODO:Refactor
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getShortTermVar() {
		return shortTermVar;
	}

	public void setShortTermVar(Integer shortTermVar) {
		this.shortTermVar = shortTermVar;
	}

	public Integer getLongTermVar() {
		return longTermVar;
	}

	public void setLongTermVar(Integer longTermVar) {
		this.longTermVar = longTermVar;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public AnnualkmEntity getAnnualkm() {
		return annualkm;
	}

	public void setAnnualkm(AnnualkmEntity annualkm) {
		this.annualkm = annualkm;
	}

	public EngagementEntity getEngagement() {
		return engagement;
	}

	public void setEngagement(EngagementEntity engagement) {
		this.engagement = engagement;
	}

	public Integer getStartPaymentMonthly() {
		return startPaymentMonthly;
	}

	public void setStartPaymentMonthly(Integer startPaymentMonthly) {
		this.startPaymentMonthly = startPaymentMonthly;
	}

	public Integer getCarAge() {
		return carAge;
	}

	public void setCarAge(Integer carAge) {
		this.carAge = carAge;
	}

	@Override
	public String toString() {
		return "AnnualkmEngagementEntity [id=" + id + ", startPaymentMonthly=" + startPaymentMonthly + ", carAge="
				+ carAge + ", shortTermVar=" + shortTermVar + ", longTermVar=" + longTermVar + ", formula=" + formula
				+ ", description=" + description + ", annualkm=" + annualkm + ", engagement=" + engagement + "]";
	}

}
