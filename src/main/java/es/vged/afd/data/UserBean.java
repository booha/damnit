package es.vged.afd.data;

import java.io.Serializable;

public class UserBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String userName;
	private Integer dealer;
	private String installationCode;
	private Integer role;
	
	public UserBean() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}


	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	public Integer getDealer() {
		return dealer;
	}

	public void setDealerId(Integer dealerId) {
		this.dealer = dealerId;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "UserBean [id=" + id + ", name=" + name + ", userName=" + userName + ", dealerId=" + dealer
				+ ", installationCode=" + installationCode + ", role=" + role + "]";
	}	
}
