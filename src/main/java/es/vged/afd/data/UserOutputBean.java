package es.vged.afd.data;

import java.io.Serializable;

public class UserOutputBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String userName;
	private DealerOutput dealer;
	private InstallationOutBean installation;
	private ProfileBean role;
	
	public UserOutputBean() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public DealerOutput getDealer() {
		return dealer;
	}

	public void setDealer(DealerOutput dealer) {
		this.dealer = dealer;
	}

	public InstallationOutBean getInstallation() {
		return installation;
	}

	public void setInstallation(InstallationOutBean installation) {
		this.installation = installation;
	}

	public ProfileBean getRole() {
		return role;
	}

	public void setRole(ProfileBean role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserOutputBean [id=" + id + ", name=" + name + ", userName=" + userName + ", dealer=" + dealer
				+ ", installation=" + installation + ", role=" + role + "]";
	}
		
}
