package es.vged.afd.data.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import es.vged.afd.data.ProfileBean;
import es.vged.afd.data.model.ProfileEntity;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProfileMapper {

	ProfileBean map(ProfileEntity profileEntity);
}
