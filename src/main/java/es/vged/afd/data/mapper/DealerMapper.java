package es.vged.afd.data.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import es.vged.afd.data.DealerOutput;
import es.vged.afd.data.model.DealerEntity;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DealerMapper {

	DealerOutput map(DealerEntity dealerOut);

}
