package es.vged.afd.data.mapper;

import java.util.List;

import es.vged.afd.data.UserBean;
import es.vged.afd.data.model.UserEntity;

public interface UserMapper {
	
	UserEntity map(UserBean valueUserBean);
	
	List<UserBean> mapList(List<UserEntity> values);
	
}
