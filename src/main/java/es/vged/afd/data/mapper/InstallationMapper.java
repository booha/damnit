package es.vged.afd.data.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import es.vged.afd.data.InstallationOutBean;
import es.vged.afd.data.model.InstallationEntity;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface InstallationMapper {

	InstallationOutBean map(InstallationEntity installationOutput);
}
