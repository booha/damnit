package es.vged.afd.data;

import java.io.Serializable;

public class DealerOutput implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String dealerCode;
	private String name;
	
	public DealerOutput() {
		super();
	}

	public DealerOutput(Integer id, String dealerCode, String name) {
		super();
		this.id = id;
		this.dealerCode = dealerCode;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDealerCode() {
		return dealerCode;
	}
	
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "DealerOutput [id=" + id + ", dealerCode=" + dealerCode + ", name=" + name + "]";
	}

}
