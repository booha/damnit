package es.vged.afd.data;

import java.io.Serializable;

public class SimulatorInput implements Serializable {

	private static final long serialVersionUID = -123456789;
	
	private Integer model;
	private Integer annualKm;
	private Integer engagement;
	
	public SimulatorInput() {
		
	}

	public SimulatorInput(Integer model, Integer annualKm, Integer engagement) {
		super();
		this.model = model;
		this.annualKm = annualKm;
		this.engagement = engagement;
	}

	public Integer getModel() {
		return model;
	}

	public void setModel(Integer model) {
		this.model = model;
	}

	public Integer getAnnualKm() {
		return annualKm;
	}

	public void setAnnualKm(Integer annualKm) {
		this.annualKm = annualKm;
	}

	public Integer getEngagement() {
		return engagement;
	}

	public void setEngagement(Integer engagement) {
		this.engagement = engagement;
	}

	@Override
	public String toString() {
		return "SimulatorInput [model=" + model + ", annualKm=" + annualKm + ", engagement=" + engagement + "]";
	}

}
