package es.vged.afd.data;

import java.io.Serializable;

public class LogsInput implements Serializable {

	private static final long serialVersionUID = -123456789;

	private Integer idUser;
	private String idInstallation;
	private int logType;
	private Double value;

	public LogsInput() {
		super();
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getIdInstallation() {
		return idInstallation;
	}

	public void setIdInstallation(String idInstallation) {
		this.idInstallation = idInstallation;
	}

	public int getLogType() {
		return logType;
	}

	public void setLogType(int logType) {
		this.logType = logType;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}