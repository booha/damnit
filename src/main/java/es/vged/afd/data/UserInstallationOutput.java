package es.vged.afd.data;

import java.io.Serializable;
import java.util.List;

public class UserInstallationOutput implements Serializable {

private static final long serialVersionUID = -123456789;	
	
	private String username;
	private String userCompleteName;
	private String installationCode;
	private String installationName;
	private Double installationLaborPrice;
	private String role;
	private List<String> authorities;

	public UserInstallationOutput() {

	}

	public UserInstallationOutput(String username, String userCompleteName, String installationCode, String installationName,
			Double installationLaborPrice, String role, List<String> authorities) {
		super();
		this.username = username;
		this.userCompleteName = userCompleteName;
		this.installationCode = installationCode;
		this.installationName = installationName;
		this.installationLaborPrice = installationLaborPrice;
		this.role = role;
		this.authorities = authorities;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserCompleteName() {
		return userCompleteName;
	}

	public void setUserCompleteName(String userCompleteName) {
		this.userCompleteName = userCompleteName;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	public String getInstallationName() {
		return installationName;
	}

	public void setInstallationName(String installationName) {
		this.installationName = installationName;
	}

	public Double getInstallationLaborPrice() {
		return installationLaborPrice;
	}

	public void setInstallationLaborPrice(Double installationLaborPrice) {
		this.installationLaborPrice = installationLaborPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String toString() {
		return "UserInstallationOutput [username=" + username + ", userCompleteName=" + userCompleteName
				+ ", installationCode=" + installationCode + ", installationName=" + installationName
				+ ", installationLaborPrice=" + installationLaborPrice + ", role=" + role + ", authorities="
				+ authorities + "]";
	}

}
