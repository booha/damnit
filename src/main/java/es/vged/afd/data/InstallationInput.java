package es.vged.afd.data;

import java.io.Serializable;

public class InstallationInput implements Serializable
{

	private static final long serialVersionUID = -123456789;

	private String installationCode;
	private String businessName;
	private Double averagePriceLabor;

	public InstallationInput()
	{
		super();
	}

	public InstallationInput(String installationCode, String businessName, Double averagePriceLabor)
	{
		super();
		this.installationCode = installationCode;
		this.businessName = businessName;
		this.averagePriceLabor = averagePriceLabor;
	}

	public String getBusinessName()
	{
		return businessName;
	}

	public void setBusinessName(String businessName)
	{
		this.businessName = businessName;
	}

	public String getInstallationCode()
	{
		return installationCode;
	}

	public void setInstallationCode(String installationCode)
	{
		this.installationCode = installationCode;
	}

	public Double getAveragePriceLabor()
	{
		return averagePriceLabor;
	}

	public void setAveragePriceLabor(Double priceLabor)
	{
		this.averagePriceLabor = priceLabor;
	}

	@Override
	public String toString()
	{
		return "InstallationInput [installationCode=" + installationCode + ", businessName=" + businessName
				+ ", averagePriceLabor=" + averagePriceLabor + "]";
	}

}
