package es.vged.afd.data;

import java.io.Serializable;

public class ModelOutput implements Serializable {
	
	private static final long serialVersionUID = -123456789;
	
	private String modelName;

	private Integer submodelId;

    private String submodelName;

    private String submodelImage;

    public ModelOutput() {

    }
    
	public ModelOutput(String modelName, Integer submodelId, String submodelName, String submodelImage) {
		super();
		this.modelName = modelName;
		this.submodelId = submodelId;
		this.submodelName = submodelName;
		this.submodelImage = submodelImage;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Integer getSubmodelId() {
		return submodelId;
	}

	public void setSubmodelId(Integer submodelId) {
		this.submodelId = submodelId;
	}

	public String getSubmodelName() {
		return submodelName;
	}

	public void setSubmodelName(String submodelName) {
		this.submodelName = submodelName;
	}

	public String getSubmodelImage() {
		return submodelImage;
	}

	public void setSubmodelImage(String submodelImage) {
		this.submodelImage = submodelImage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
