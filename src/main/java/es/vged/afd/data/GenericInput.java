package es.vged.afd.data;

import java.io.Serializable;

public class GenericInput implements Serializable {
	
	private static final long serialVersionUID = -123456789;

	private Integer id;
	private String name;
	
	public GenericInput() { //NOSONAR //TODO:Refactor
		
	}
	
	public GenericInput(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InstallationInput [id=" + id + ", name=" + name + "]";
	}

}
