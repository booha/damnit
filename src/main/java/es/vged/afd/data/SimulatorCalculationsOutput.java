package es.vged.afd.data;

import java.io.Serializable;

public class SimulatorCalculationsOutput implements Serializable{

	private static final long serialVersionUID = -123456789;
	
	private Double feeAfd;
	private Double longCost;
	private Double shortCost;
	private Integer monthPromotion;
	private Double copayment;
	
	private String longTermSavings;
	private String shortTermSavings;
	
	
	public SimulatorCalculationsOutput() { //NOSONAR //TODO:Refactor
		
	}
	
	public SimulatorCalculationsOutput(Double feeAfd, Double longCost, Double shortCost, Integer monthPromotion, 
										Double copayment, String longTermSavings, String shortTermSavings) {
		
		this.feeAfd = feeAfd;
		this.longCost = longCost;
		this.shortCost = shortCost;
		this.monthPromotion = monthPromotion;
		this.copayment = copayment;
		this.longTermSavings = longTermSavings;
		this.shortTermSavings = shortTermSavings;

	}
	
	
	public Double getFeeAfd() {
		return feeAfd;
	}
	public void setFeeAfd(Double feeAfd) {
		this.feeAfd = feeAfd;
	}
	public Double getLongCost() {
		return longCost;
	}
	public void setLongCost(Double longCost) {
		this.longCost = longCost;
	}
	public Double getShortCost() {
		return shortCost;
	}
	public void setShortCost(Double shortCost) {
		this.shortCost = shortCost;
	}
	public Integer getMonthPromotion() {
		return monthPromotion;
	}
	public void setMonthPromotion(Integer monthPromotion) {
		this.monthPromotion = monthPromotion;
	}
	public Double getCopayment() {
		return copayment;
	}
	public void setCopayment(Double copayment) {
		this.copayment = copayment;
	}
	public String getLongTermSavings() {
		return longTermSavings;
	}
	public void setLongTermSavings(String longTermSavings) {
		this.longTermSavings = longTermSavings;
	}
	public String getShortTermSavings() {
		return shortTermSavings;
	}
	public void setShortTermSavings(String shortTermSavings) {
		this.shortTermSavings = shortTermSavings;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
