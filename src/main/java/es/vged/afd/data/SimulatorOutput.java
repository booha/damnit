package es.vged.afd.data;

import java.io.Serializable;


public class SimulatorOutput implements Serializable {

	private static final long serialVersionUID = -123456789;

	private Integer planId;
	private String planName;
	private SimulatorCalculationsOutput simulatorCalculations;
	
	public SimulatorOutput() {
		
	}
	
	public SimulatorOutput(Integer planId, String planName, SimulatorCalculationsOutput simulatorCalculations) {
		this.planId = planId;
		this.planName = planName;
		this.simulatorCalculations = simulatorCalculations;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public SimulatorCalculationsOutput getSimulatorCalculations() {
		return simulatorCalculations;
	}

	public void setSimulatorCalculations(SimulatorCalculationsOutput simulatorCalculations) {
		this.simulatorCalculations = simulatorCalculations;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
