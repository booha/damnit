package es.vged.afd.data;

import java.io.Serializable;

public class ProfileOutput implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	
	public ProfileOutput() {
		super();
	}

	public ProfileOutput(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "ProfileOutput [id=" + id + ", name=" + name + "]";
	}

}
