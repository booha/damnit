package es.vged.afd.data;

import java.io.Serializable;

import es.vged.afd.data.model.DealerEntity;
import es.vged.afd.data.model.ProfileEntity;

public class UserInput implements Serializable {
	
	private static final long serialVersionUID = -123456789;
	
	private String username;
	private String name;
	private DealerEntity dealer;
	private String installationCode;
	private ProfileEntity role;

	public UserInput() {
		
	}
	public UserInput(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DealerEntity getDealer() {
		return dealer;
	}
	public void setDealer(DealerEntity dealer) {
		this.dealer = dealer;
	}
	public String getInstallationCode() {
		return installationCode;
	}
	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}
	public ProfileEntity getRole() {
		return role;
	}
	public void setRole(ProfileEntity role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "UserInput [username=" + username + ", name=" + name + ", dealer=" + dealer + ", installationCode="
				+ installationCode + ", role=" + role + "]";
	}	

}
