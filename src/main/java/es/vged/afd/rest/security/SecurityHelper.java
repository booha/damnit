package es.vged.afd.rest.security;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.vged.afd.common.audit.LoggerHelper;
import es.vged.afd.config.SecurityOptions;
import es.vged.afd.data.model.ProfileAuthorityEntity;
import es.vged.afd.data.model.ProfileEntity;
import es.vged.afd.data.model.UserEntity;
import es.vged.afd.service.security.UserJWT;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public final class SecurityHelper //NOSONAR //TODO:Refactor
{	
	private static final LoggerHelper LOGGER = LoggerHelper.getLoggerHelper(SecurityHelper.class);

	public static final String SYSTEM_USER = "SYSTEM";
	
	/* Sacar esta clave al archivo de claves */
	public static final String JWT_SECRET = "RBqnJ4MM6t20lFtDaLPRoGyIKG8cExTl";
	
	@Autowired
	static SecurityOptions securityOptions;
	
	@SuppressWarnings("unchecked")
	public static String getUserFromSsoToken(String ssoToken)
	{
		byte[] playloadDecode = Base64.getDecoder().decode(ssoToken.split("\\.")[1]);
		String decodedString = new String(playloadDecode); //NOSONAR //TODO:Refactor
		
		HashMap<String,Object> result = null;
	    try {
	    	result = new ObjectMapper().readValue(decodedString, HashMap.class);

	    } catch (Exception e) { //NOSONAR //TODO:Refactor
	    	e.printStackTrace(); //NOSONAR //TODO:Refactor
		}
	    
	    if ( result != null ) {
	    	return (String) result.get("preferred_username");
	    } else {
	    	return null;
	    }
	}
	
	public static String createToken(UserEntity user)
	{
		return Jwts.builder()
				.claim("userName", user.getUsername())
				.claim("nro", user.getId())
				.claim("installation", user.getInstallation())
				.claim("completeUserName", user.getUserCompleteName())
				.claim("grantedAuthorities", getUserGrantedAuthoritiesString(user)).setIssuedAt(new Date())
				.setIssuedAt(new Date()) 
			    .signWith(SignatureAlgorithm.HS256, JWT_SECRET)
			    .compact();
	}
	
	@SuppressWarnings({ "unchecked" })
	public static UserJWT validateToken(String token) 
	{
		UserJWT retorno = null;

		try 
		{
			Claims claim = Jwts.parser()
				    .setSigningKey(JWT_SECRET)
				    .parseClaimsJws(token)
				    .getBody();
			
			List<String> grantedAuthorities = claim.get("grantedAuthorities", List.class);
			
			List<GrantedAuthority> grantedAuthoritiesParsed = new ArrayList<>();
			for (String grantedAuthority : grantedAuthorities) {
				grantedAuthoritiesParsed.add(new SimpleGrantedAuthority(grantedAuthority));
			}
			
			retorno = new UserJWT(claim.get("nro", Long.class),claim.get("userName",String.class),grantedAuthoritiesParsed);			
			retorno.setName((String) claim.get("completeUserName"));			
			
		} catch (Exception e) //NOSONAR //TODO:Refactor
		{
			LOGGER.error("Invalid User token: " + token);
			e.printStackTrace(); //NOSONAR //TODO:Refactor
		}
		
		return retorno;
	}

	/**
	 * Gets Security principal Object
	 * @return the Principal
	 */
	public static UserJWT getPrincipal() {
		return (UserJWT)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	
	/**
	 * Gets current authenticated user
	 * 
	 * @return the user principal (username)
	 */
	public static String getPrincipalUsername()
	{
		String username;
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null || !authentication.isAuthenticated())
		{
			username = SYSTEM_USER;
		} else
		{
			username = ((UserJWT)authentication.getPrincipal()).getUsername();
		}

		return username;

	}
	
	private static List<GrantedAuthority> getUserGrantedAuthorities(UserEntity user) {
		// montamos la lista de permisos del usuario a partir de sus perfiles
		List<String> grantedAuthorities = new ArrayList<>();

		// añadimos profiles y permisos de cada profile
		for (ProfileEntity profile : user.getProfilesList()) {
			grantedAuthorities.addAll(getGrantedAuthorities(profile));
		}

		// retornamos los permisos para Spring Security eliminando duplicados
		return deleteDuplicates(grantedAuthorities);
	}

	private static List<String> getGrantedAuthorities(ProfileEntity profile) {
		List<String> grantedAuthorities = new ArrayList<>();
		for (ProfileAuthorityEntity pa : profile.getProfileAuthorities()) {

			grantedAuthorities.add(pa.getAuthority().getName());
		}

		return grantedAuthorities;
	}
	
	private static List<String> getUserGrantedAuthoritiesString(UserEntity user) {
		List<GrantedAuthority> authorities = getUserGrantedAuthorities(user);
		
		List<String> grantedAuthorities = new ArrayList<>();
		for (GrantedAuthority auth : authorities) {

			grantedAuthorities.add(auth.getAuthority());
		}

		return grantedAuthorities;
	}

	private static List<GrantedAuthority> deleteDuplicates(List<String> repeatedGrantedAuthorities) {
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		Set<String> unrepeatedRepeatedAuthorities = new HashSet<>();

		for (String grantedAuthority : repeatedGrantedAuthorities) {
			if (unrepeatedRepeatedAuthorities.add(grantedAuthority)) // elimina duplicados
			{
				grantedAuthorities.add(new SimpleGrantedAuthority(grantedAuthority));
			}
		}

		return grantedAuthorities;
	}

	public static boolean hasAuthority(String authority, List<GrantedAuthority> authorities) {
		boolean hasAuthority = false;

		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals(authority)) {
				hasAuthority = true;
				break;
			}
		}
		return hasAuthority;
	}
}
