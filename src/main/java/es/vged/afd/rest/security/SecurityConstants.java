package es.vged.afd.rest.security;

public class SecurityConstants {

	public static final String AUTH_MANAGE_OFFERS = "Manage offers";
	public static final String AUTH_MODIFY_LABOR_PRICE = "Modify labor price";
	public static final String AUTH_MANAGE_USERS = "Manage users";
}
