package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.PlanEngagementEntity;


@Repository
public interface PlanEngagementRepository extends JpaRepository<PlanEngagementEntity, Integer>  {

	@Query(value = "select plan_engag0_.* from dbo.plan_engagement plan_engag0_  left outer join dbo.engagement engagement1_ on plan_engag0_.engagement_id=engagement1_.id  "
			+ "inner join dbo.afdplan plan2_ on plan_engag0_.plan_id = plan2_.id "
			+ "where engagement1_.id = ?1 order by plan2_.order_plan ASC;", nativeQuery = true)
	List<PlanEngagementEntity>  getByEngagementIdAndOrderByPlanOrder(Integer engagementId);
	
	
}
