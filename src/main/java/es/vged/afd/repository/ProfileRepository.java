package es.vged.afd.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ProfileEntity;


@Repository
public interface ProfileRepository extends JpaRepository<ProfileEntity, Integer> {
	
	List<ProfileEntity> findAll();

}
