package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import es.vged.afd.data.model.LogsEntity;

/**
 * @author fernanda.goncalves
 *
 */
@Repository
public interface LogsRepository extends JpaRepository<LogsEntity, Long> {

	
	
	
}
