package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.PlanEntity;

@Repository
public interface PlanRepository extends JpaRepository<PlanEntity, Integer>{

	PlanEntity findOneById(Integer id);

	List<PlanEntity> findAll();
}
