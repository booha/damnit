package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ArgumentEntity;

@Repository
public interface ArgumentRepository extends JpaRepository<ArgumentEntity, Integer> {

	List<ArgumentEntity> findByChannelId(Integer channelId);
}
