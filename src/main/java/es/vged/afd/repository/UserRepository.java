package es.vged.afd.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.vged.afd.data.model.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
	
	UserEntity findOneByUsername(String username);

	@Transactional
	@Modifying
	@Query(value = "UPDATE AFDUSER SET [LAST_ACCESS_DATE] = ?1  WHERE  [USERNAME] = ?2", nativeQuery = true)
	void updateLastDateAccess(Date lastdate, String username);

	@Query(value ="SELECT ue FROM UserEntity ue WHERE ue.username LIKE %?1%")
	List<UserEntity> getUsersByUserName(String name);

	void deleteById(Integer id);

	UserEntity findByUsername(String username);
	
}
