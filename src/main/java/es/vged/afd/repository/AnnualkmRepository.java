package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.AnnualkmEntity;

@Repository
public interface AnnualkmRepository extends JpaRepository<AnnualkmEntity, Integer> {
	
	List<AnnualkmEntity> findAllByOrderByNameAsc();

}
