package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.EngagementEntity;

@Repository
public interface EngagementRepository extends JpaRepository<EngagementEntity, Integer> {

	public List<EngagementEntity> findByChannelIdOrderByNameAsc(Integer channelId);

}
