package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.BenefitEntity;

@Repository
public interface BenefitRepository extends JpaRepository<BenefitEntity, Integer> {
	
	List<BenefitEntity> findByChannelId(Integer channelId);
}
