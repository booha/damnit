package es.vged.afd.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.vged.afd.data.model.InstallationEntity;

@Repository
public interface InstallationRepository  extends JpaRepository<InstallationEntity, Integer>{

	InstallationEntity findOneByInstallationCode(String id);

	@Transactional
	@Modifying
	@Query(value = "UPDATE InstallationEntity SET averagePriceLabor = ?1, lastModifiedBy = ?2, lastModifiedDate = ?3 WHERE installationCode = ?4")
	void updateAvgPriceLabor(Double avgPriceLabor, String user, Date date, String installationCode);
	
	List<InstallationEntity> findByDealerCode(String dealerCode);

	List<InstallationEntity> findInstallationByDealerCode(String dealerCode);
	
	List<InstallationEntity> findAll();
	
	InstallationEntity findOneByDealerCode (String dealerCode);
}
