package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ParameterEntity;

@Repository
public interface ParameterRepository  extends JpaRepository<ParameterEntity, Integer> {

	ParameterEntity findByName(String name);
	
}
