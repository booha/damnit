package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.UserProfileEntity;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfileEntity, Integer>, CrudRepository<UserProfileEntity, Integer> {

	void deleteByPkUserId(Integer id);
	
	UserProfileEntity findByPkUserId (Integer id);

}
