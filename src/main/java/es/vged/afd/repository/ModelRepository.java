package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ModelEntity;

@Repository
public interface ModelRepository extends JpaRepository<ModelEntity, Integer> {

	ModelEntity findOneById(Integer id);

}