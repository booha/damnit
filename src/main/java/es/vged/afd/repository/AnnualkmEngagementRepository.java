package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.AnnualkmEngagementEntity;

@Repository	
public interface AnnualkmEngagementRepository extends JpaRepository<AnnualkmEngagementEntity, Integer> {

	AnnualkmEngagementEntity findByAnnualkmIdAndEngagementId(Integer annualkmId, Integer engagementId);
}
