package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ChannelEntity;

@Repository
public interface ChannelRepository extends JpaRepository<ChannelEntity, Integer> {

	
}
