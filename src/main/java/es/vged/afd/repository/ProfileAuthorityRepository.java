package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.ProfileAuthorityEntity;


@Repository
public interface ProfileAuthorityRepository extends JpaRepository<ProfileAuthorityEntity, Long> 
{

	List<ProfileAuthorityEntity> findByPkProfileId(Integer id);
}
