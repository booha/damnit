package es.vged.afd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.PlanEngagementEntity;
import es.vged.afd.data.model.SimulatorEntity;

@Repository
public interface SimulatorRepository extends JpaRepository<SimulatorEntity, Integer>{

	SimulatorEntity findByPlanengagementAndModelIdAndAnnualkmId(PlanEngagementEntity planengagement, Integer modelId, Integer annualkmId);

}

