package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.DealerEntity;
@Repository
public interface DealerRepository extends JpaRepository<DealerEntity, Integer>{

	DealerEntity findByDealerCode(String dealerCode);
	
	List<DealerEntity> findAll();

}
