package es.vged.afd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.vged.afd.data.model.CoverageEntity;

@Repository
public interface CoverageRepository extends JpaRepository<CoverageEntity, Integer> {
	
	List<CoverageEntity> findByChannelId(Integer channelId);
}
