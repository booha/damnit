package es.vged.afd.common.audit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Utility class for logging. <br/>
 * <b>Usage: </b>{@code LoggerHelper logger = LoggerHelper.getLoggerHelper(AClass.class);}
 *
 * @author alexis.mendez
 * @see Logger
 */
public final class LoggerHelper  //NOPMD
{

	public static final String ANONYMOUS_USER = "Anonymous";

	private final Logger logger; // NOPMD by antonio.perez on 8/06/16 14:22

	private static final int MAX_RESULT_SIZE = 5000;

	private LoggerHelper(Class<?> clazz)
	{
		this.logger = LoggerFactory.getLogger(clazz);
	}

	/**
	 * Creates a logger instance for the given class
	 *
	 * @param clazz
	 *            class to be logged
	 * @return a {@code LoggerHelper} instance
	 */
	public static LoggerHelper getLoggerHelper(Class<?> clazz)
	{
		return new LoggerHelper(clazz);
	}

	/**
	 * Log the begin of given methodName with debug level if DEBUG level is enabled.
	 *
	 * @param username
	 *            username of the cuirrent user connected
	 * @param methodName
	 *            the method name to log.
	 * @param arguments
	 *            the method's list of arguments
	 */
	public void begin(String username, String methodName, Object... arguments)
	{

		StringBuilder builder = new StringBuilder();
		builder.append("Begin: ");
		builder.append(this.getMethodName(methodName));
		builder.append(" Executing as: ");
		builder.append(username);
		if (this.isDebugEnabled())
		{
			builder.append(" Arguments: ");
			builder.append(this.getMethodArguments(arguments));
		}

		this.info(builder.toString());
	}

	/**
	 * Log the begin of given methodName with debug level if DEBUG level is enabled.
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param arguments
	 *            the method's list of arguments
	 */
	public void begin(String methodName, Object... arguments)
	{
		this.begin(getUsername(), methodName, arguments);
	}

	/**
	 * Log the begin of given methodName with debug level if DEBUG level is enabled.
	 * 
	 * @param methodName
	 *            the method name to log.
	 */
	public void begin(String methodName)
	{
		this.begin(getUsername(), methodName);
	}

	/**
	 * Log the end of given methodName with debug level if DEBUG level is enabled.
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param methodResult
	 *            the result of the method execution
	 * @param error
	 *            any exception raised during method execution
	 * @param timeInMillis
	 *            duration of the method execution in milliseconds
	 */
	public void end(String methodName, Object methodResult, Throwable error, Long timeInMillis)
	{

		StringBuilder builder = new StringBuilder();

		builder.append("End with ");
		builder.append(this.getMethodName(methodName));

		appendResult(methodResult, error, builder);

		builder.append(this.getMethodExecutionTime(timeInMillis));
		this.info(builder.toString());

	}

	private void appendResult(Object methodResult, Throwable error, StringBuilder builder)
	{
		if (error == null)
		{
			builder.append(" Result: ");
			String result = this.getMethodResult(methodResult);
			if (result.length() > MAX_RESULT_SIZE)
			{
				result = result.substring(0, MAX_RESULT_SIZE) + "...";
			}
			builder.append(result);
		} else if (error != null)
		{
			builder.append(" Exception: ");
			builder.append(error.getClass().getName());
			if (error.getCause() != null)
			{
				builder.append('\n');
				builder.append(ExceptionUtils.getStackTrace(error.getCause()));
			}
		}
	}

	/**
	 * Log the end of given methodName with debug level if DEBUG level is enabled.
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param timeInMillis
	 *            duration of the method execution in milliseconds
	 */
	public void end(String methodName, Long timeInMillis)
	{
		this.end(methodName, null, null, timeInMillis);
	}

	/**
	 * Log the end of given methodName with debug level if DEBUG level is enabled.
	 *
	 * @param methodName
	 *            the method name to log.
	 */
	public void end(String methodName)
	{
		this.end(methodName, null, null, null);
	}

	/**
	 * Log the execution of the given methodName with INFO level.
	 *
	 * @param userName
	 *            User logged in
	 * @param methodName
	 *            the method name to log.
	 * @param arguments
	 *            the method's list of arguments
	 * @param result
	 *            the result of the method execution
	 * @param error
	 *            any exception raised during method execution
	 * @param timeInMillis
	 *            duration of the method execution in milliseconds
	 */
	public void call(String userName, String methodName, Object[] arguments, Object result, Throwable error,
			Long timeInMillis) //NOSONAR //TODO:Refactor
	{
		StringBuilder builder = new StringBuilder();
		builder.append("Call on ");
		builder.append(this.getMethodName(methodName));
		builder.append(" Executed as: ");
		builder.append(userName);
		builder.append(" with Arguments: ");
		builder.append(this.getMethodArguments(arguments));
		builder.append(" -> ENDED with ");
		this.appendResult(result, error, builder);
		this.info(builder.toString());
	}

	/**
	 * Log a exception with error level. <br/>
	 * <p>
	 * This method will log a error message with detailed information of the exception, methodName and possible
	 * additional information message and stack trace.
	 * </p>
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param throwable
	 *            the exception to log.
	 * @param additionalInfo
	 *            additional information message.
	 * @param printStackTrace
	 *            true for include StackTrace otherwise false.
	 */
	public void exception(String methodName, Throwable throwable, String additionalInfo, boolean printStackTrace)
	{
		StringBuffer message = new StringBuffer(); //NOSONAR //TODO:Refactor

		message.append(methodName);
		message.append(" method: Exception thrown");

		if (throwable != null)
		{
			message.append(throwable.getClass());
			message.append(" with message \"");
			message.append(throwable.getMessage());
			message.append('"');
		}

		if (StringUtils.isNotBlank(additionalInfo))
		{
			message.append(". Additional information: ");
			message.append(additionalInfo);
		}

		this.logger.error(message.toString()); //NOSONAR //TODO:Refactor

		if (printStackTrace && throwable instanceof Exception)
		{
			this.printStackTrace((Exception) throwable);
		}
	}

	/**
	 * Print formatted stacktrace of an exception
	 *
	 * @param exception
	 *            Exception instance to be logged
	 */
	private void printStackTrace(Exception exception)
	{
		if (exception != null)
		{
			StackTraceElement[] elem = exception.getStackTrace();
			for (int i = 0; i < elem.length; i++)
			{
				logger.error("		@STACKTRACE[" + i + "]: " + elem[i].toString()); //NOSONAR //TODO:Refactor
			}
		}
	}

	/**
	 * Log a exception with error level. <br/>
	 * <p>
	 * This method will log a error message with detailed information of the exception, methodName and possible stack
	 * trace information
	 * </p>
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param throwable
	 *            the exception to log.
	 * @param printStackTrace
	 *            true for include StackTrace otherwise false.
	 */
	public void exception(String methodName, Throwable throwable, boolean printStackTrace)
	{
		this.exception(methodName, throwable, null, printStackTrace);
	}

	/**
	 * Log a exception with error level. <br/>
	 * <p>
	 * This method will log a error message with information about the exception and methodName.
	 * </p>
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param throwable
	 *            the exception to log.
	 */
	public void exception(String methodName, Throwable throwable)
	{
		this.exception(methodName, throwable, null, false);
	}

	/**
	 * Log a exception with error level. <br/>
	 * <p>
	 * This method will log a error message with detailed information of about exception and methodName
	 * </p>
	 *
	 * @param methodName
	 *            the method name to log.
	 * @param throwable
	 *            the exception to log.
	 * @param additionalInfo
	 *            additional information message.
	 */
	public void exception(String methodName, Throwable throwable, String additionalInfo)
	{
		this.exception(methodName, throwable, additionalInfo, false);
	}

	/**
	 * @see org.slf4j.Logger#debug(String)
	 */
	public void debug(String message)
	{
		if (logger.isDebugEnabled())
		{
			this.logger.debug(message);
		}
	}

	/**
	 * @see org.apache.logging.log4j.Logger#debug(String, Throwable)
	 */
	public void debug(String message, Throwable t)
	{
		if (logger.isDebugEnabled())
		{
			this.logger.debug(message, t);
		}
	}

	/**
	 * @see org.slf4j.Logger#error(String)
	 */
	public void error(String message)
	{
		this.logger.error(message);
	}

	/**
	 * @see org.slf4j.Logger#error(String, Throwable)
	 */
	public void error(String message, Throwable t)
	{
		this.logger.error(message, t);
	}

	/**
	 * @see org.slf4j.Logger#info(String)
	 */
	public void info(String message)
	{
		this.logger.info(message);
	}

	/**
	 * @see org.slf4j.Logger#info(String, Throwable)
	 */
	public void info(String message, Throwable t)
	{
		this.logger.info(message, t);
	}

	/**
	 * @see org.slf4j.Logger#isDebugEnabled()
	 */
	public boolean isDebugEnabled()
	{
		return this.logger.isDebugEnabled();
	}

	/**
	 * @see org.slf4j.Logger#isInfoEnabled()
	 */
	public boolean isInfoEnabled()
	{
		return this.logger.isInfoEnabled();
	}

	/**
	 * Check whether @see org.slf4j.Level#WARN is enabled.
	 *
	 * @return <code>true</code> if WARN is, <code>false</code> otherwise.
	 */
	public boolean isWarnEnabled()
	{
		return this.logger.isWarnEnabled();
	}

	/**
	 * @see org.slf4j.Logger#warn(String)
	 */
	public void warn(String message)
	{
		this.logger.warn(message);
	}

	/**
	 * @see org.slf4j.Logger#warn(String, Throwable)
	 */
	public void warn(String message, Throwable t)
	{
		this.logger.warn(message, t);
	}

	private String getMethodName(String methodName)
	{
		return StringUtils.isNotBlank(methodName) ? methodName : "unspecifiedMethod";
	}

	private String getMethodArguments(Object... arguments)
	{
		String result = "none";
		if (arguments != null && arguments.length > 0)
		{
			result = this.serializeObject(arguments);
		}
		return result;
	}

	private String getMethodExecutionTime(Long timeInMillis)
	{
		StringBuilder builder = new StringBuilder(". Execution TIME: ");
		if (timeInMillis != null && timeInMillis > 0)
		{
			builder.append(timeInMillis);
			builder.append(" milliseconds.");
		} else
		{
			builder.append("not evaluated.");
		}
		return builder.toString();
	}

	private String getMethodResult(Object methodResult)
	{
		StringBuilder builder = new StringBuilder("void");
		if (methodResult != null)
		{
			builder = new StringBuilder();
			if (methodResult instanceof Collection)
			{
				int originalSize = ((Collection<?>) methodResult).size();
				builder.append("Collection with ");
				builder.append(originalSize);
				builder.append(" elements.");
				if (this.isDebugEnabled())
				{
					Collection<?> toPrint = this.getElementsToPrint((Collection<?>) methodResult);
					builder.append(" Printing ");
					builder.append(toPrint.size());
					builder.append(" of ");
					builder.append(originalSize);
					builder.append(": ");
					builder.append(this.serializeObject(toPrint));
				}
			} else
			{
				builder.append(methodResult.getClass().getName());
				builder.append(": ");
				builder.append(this.serializeObject(methodResult));
			}
		}
		return builder.toString();
	}

	@SuppressWarnings("unchecked")
	private Collection<?> getElementsToPrint(Collection<?> collection)
	{
		@SuppressWarnings("rawtypes")
		Collection result = new ArrayList<>();
		Iterator<?> iterator = collection.iterator();
		while (iterator.hasNext() && result.size() <= 10)
		{
			result.add(iterator.next());
		}
		return result;
	}

	private String serializeObject(Object object) //NOSONAR //TODO:Refactor
	{
		return "Serialization Disabled!."; //NOSONAR //TODO:Refactor
	}

	private String getUsername()
	{

		String username = LoggerHelper.ANONYMOUS_USER;
		if (SecurityContextHolder.getContext() != null
				&& SecurityContextHolder.getContext().getAuthentication() != null)
		{
			username = SecurityContextHolder.getContext().getAuthentication().getName();
		}
		return username;
	}

}