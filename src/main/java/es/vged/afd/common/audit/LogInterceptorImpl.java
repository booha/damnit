package es.vged.afd.common.audit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import es.vged.afd.common.misc.ElapsedTime;

/**
 * Class responsible for logging of information related with methods execution.
 */
@Component("logInterceptor")
public class LogInterceptorImpl implements LogInterceptor
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object logMethodCall(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		return this.logAroundInformation(joinPoint, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void logMethodBegin(final JoinPoint joinPoint)
	{
		this.getLogger(joinPoint).begin(getUsername(), this.getFullMethodName(joinPoint), joinPoint.getArgs());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void logMethodEndSuccessful(JoinPoint joinPoint, Object result)
	{
		this.getLogger(joinPoint).end(this.getFullMethodName(joinPoint), result, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void logMethodEndFailure(JoinPoint joinPoint, Exception exception)
	{
		this.getLogger(joinPoint).end(this.getFullMethodName(joinPoint), null, exception, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object logMethodBeginAndEnd(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		return this.logAroundInformation(joinPoint, true);
	}

	/**
	 * Encapsulates the creation of the log for the Around advices
	 *
	 * @param joinPoint
	 * @param logAtBeginAndEnd
	 *            True add Log at the beginning and at the end of the method execution. Otherwise only add log at the
	 *            end.
	 * @return
	 * @throws Throwable
	 */
	private Object logAroundInformation(ProceedingJoinPoint joinPoint, boolean logAtBeginAndEnd) throws Throwable
	{
		ElapsedTime elapsedTime = new ElapsedTime();

		Object methodResult = null;
		Throwable methodError = null;
		String methodFullName = null;
		Object[] methodArguments = null;
		String userName = getUsername();
		try
		{
			methodFullName = this.getFullMethodName(joinPoint);
			methodArguments = joinPoint.getArgs();
			if (logAtBeginAndEnd)
			{
				this.getLogger(joinPoint).begin(userName, methodFullName, methodArguments);
			}
			methodResult = joinPoint.proceed(methodArguments);
		} catch (Throwable e)// NOPMD
		{ 
			methodError = e;
			throw e;
		} finally
		{
			long elapseTimeMillis = elapsedTime.getElapseTimeMillis();
			if (logAtBeginAndEnd)
			{
				this.getLogger(joinPoint).end(methodFullName, methodResult, methodError, elapseTimeMillis);
			} else
			{
				this.getLogger(joinPoint).call(userName, methodFullName, methodArguments, methodResult, methodError,
						elapseTimeMillis);
			}
		}

		return methodResult;
	}

	/**
	 * Get the full method name: class name + method name
	 *
	 * @param joinPoint
	 * @return
	 */
	private String getFullMethodName(final JoinPoint joinPoint)
	{
		return joinPoint.getTarget().getClass().getName() + "#" + joinPoint.getSignature().getName();
	}

	private LoggerHelper getLogger(final JoinPoint joinPoint)
	{
		return LoggerHelper.getLoggerHelper(joinPoint.getTarget().getClass());
	}

	private String getUsername()
	{

		String username = LoggerHelper.ANONYMOUS_USER;
		if (SecurityContextHolder.getContext() != null
				&& SecurityContextHolder.getContext().getAuthentication() != null)
		{
			username = SecurityContextHolder.getContext().getAuthentication().getName();
		}
		return username;
	}

}
