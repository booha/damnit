package es.vged.afd.common.audit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Allows logging method's execution information
 */
public interface LogInterceptor
{

	/**
	 * Interceptor method to log the information (name, arguments, return and execution time) associated to a method
	 * complete execution. This method is intended to be used in an Around advice.
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	Object logMethodCall(final ProceedingJoinPoint joinPoint) throws Throwable; //NOSONAR //TODO:Refactor

	/**
	 * Interceptor method to log the information (name, arguments) associated to the start of a method execution. This
	 * method is intended to be used in a Before advice.
	 *
	 * @param joinPoint
	 */
	void logMethodBegin(final JoinPoint joinPoint);

	/**
	 * Interceptor method to log the information (name, return) associated to the end of a successful method execution.
	 * This method is intended to be used in an AfterReturning advice.
	 *
	 * @param joinPoint
	 * @param result
	 */
	void logMethodEndSuccessful(final JoinPoint joinPoint, final Object result);

	/**
	 * Interceptor method to log the information (name, error) associated to the end of a failure method execution. This
	 * method is intended to be used in an AfterThrowing advice.
	 *
	 * @param joinPoint
	 * @param exception
	 */
	void logMethodEndFailure(final JoinPoint joinPoint, final Exception exception);

	/**
	 * Interceptor method to log the information (name, return/error, execution time) associated to the start and end of
	 * a method execution (successful or not). This method is intended to be used in an Around advice.
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	Object logMethodBeginAndEnd(final ProceedingJoinPoint joinPoint) throws Throwable; //NOSONAR //TODO:Refactor
}
