package es.vged.afd.common.misc;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import es.vged.afd.common.audit.LoggerHelper;

/**
 * Utility methods for objects serialization/deserialization
 * 
 * @author alexis.mendez
 */
public final class SerializationUtils
{

	private static final LoggerHelper LOGGER = LoggerHelper.getLoggerHelper(SerializationUtils.class);

	/**
	 * Singleton Jackson {@link com.fasterxml.jackson.databind.ObjectMapper} instance
	 */
	private static ObjectMapper jacksonJsonSerializer;

	static
	{
		jacksonJsonSerializer = new ObjectMapper();
		jacksonJsonSerializer.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		jacksonJsonSerializer.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	private SerializationUtils()
	{
		// Do nothing. Just to avoid instantiation
	}

	/**
	 * Returns an instance of {@link com.fasterxml.jackson.databind.ObjectMapper}. The mapper is singleton and is
	 * configured not to fail on empty beans.
	 * 
	 * @return {@link com.fasterxml.jackson.databind.ObjectMapper}
	 */
	public static ObjectMapper getJacksonJsonSerializerInstance()
	{
		return jacksonJsonSerializer;
	}

	/**
	 * Serializes the given object to JSON string
	 * 
	 * @param object
	 *            object to serialize
	 * @return JSON
	 * @throws IOException
	 *             if any serialization problem occurs
	 */
	public static String serializeToJsonString(Object object) throws IOException
	{
		try
		{
			return getJacksonJsonSerializerInstance().writeValueAsString(object);
		} catch (JsonProcessingException e)
		{
			LOGGER.exception("Error serializing object to Json", e);
			throw e;
		}
	}

	/**
	 * Serializes the given object to JSON and writes the result to the given file.
	 * 
	 * @param object
	 *            object to serialize
	 * @param targetJasonFilePath
	 *            complete path to the JSON file to save
	 * @throws IOException
	 *             if any serialization problem occurs
	 */
	public static void serializeToJsonFile(Object object, String targetJasonFilePath) throws IOException
	{
		try
		{
			getJacksonJsonSerializerInstance().writeValue(new File(targetJasonFilePath), object);
		} catch (JsonProcessingException e)
		{
			LOGGER.exception("Error serializing object to Json", e);
			throw e;
		}
	}

	/**
	 * Deserializes the given JSON string to the specified object type
	 * 
	 * @param json
	 *            JSON string to deserialize
	 * @param objectType
	 *            the class of the object expected from the deserialization
	 * @param <T>
	 *            the type of the expected object
	 * @return The object represented by the json
	 * @throws IOException
	 *             if any deserialization error occurs
	 */
	public static <T> T deserializeFromJsonString(String json, Class<T> objectType) throws IOException
	{
		try
		{
			return getJacksonJsonSerializerInstance().readValue(json, objectType);
		} catch (IOException e)
		{
			LOGGER.exception("Error deserializing from Json", e);
			throw e;
		}
	}

	/**
	 * Deserializes the given JSON file to the specified object type
	 * 
	 * @param sourceJsonFilePath
	 *            complete path to the JSON file to read
	 * @param objectType
	 *            the class of the object expected from the deserialization
	 * @param <T>
	 *            the type of the expected object
	 * @return The object that represent the content of the file
	 * @throws IOException
	 *             if any deserialization error occurs
	 */
	public static <T> T deserializeFromJsonFile(String sourceJsonFilePath, Class<T> objectType) throws IOException
	{
		try
		{
			return getJacksonJsonSerializerInstance().readValue(new File(sourceJsonFilePath), objectType);
		} catch (IOException e)
		{
			LOGGER.exception("Error deserializing fom Json", e);
			throw e;
		}
	}

}
