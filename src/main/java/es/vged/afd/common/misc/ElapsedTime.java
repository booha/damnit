package es.vged.afd.common.misc;

/**
 * Utility class that allows us to know the time between the instantiation and the invocation of one of its methods
 * getElapseTime() or getElapseTimeMillis()
 */
public class ElapsedTime
{

	private final long startTime;

	/**
	 * Default constructor. Register the current time.
	 */
	public ElapsedTime()
	{
		this.startTime = System.currentTimeMillis();
	}

	/**
	 * @return a String containing the elapsed time in seconds.
	 */
	public String getElapseTime()
	{
		long duration = System.currentTimeMillis() - startTime;
		float value = ((float)duration) / 1000;
		return String.format("%10.2f seconds", value);
	}

	/**
	 * @return the elapsed time in milliseconds.
	 */
	public long getElapseTimeMillis()
	{
		return System.currentTimeMillis() - startTime;
	}

}
