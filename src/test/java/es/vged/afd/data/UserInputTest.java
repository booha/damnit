package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.DealerEntity;
import es.vged.afd.data.model.ProfileEntity;

@RunWith(MockitoJUnitRunner.class)
public class UserInputTest {
	
	@Test
	public void UserInputTest() {
		
		UserInput userInput = new UserInput();
		
		String username = "userName";
	 	String name = "name";
	 	DealerEntity dealer = new DealerEntity();
	 	String installationCode = "1234a";
	 	ProfileEntity role = new ProfileEntity();
	 	
	 	userInput.setDealer(dealer);
	 	userInput.setInstallationCode(installationCode);
	 	userInput.setName(name);
	 	userInput.setRole(role);
	 	userInput.setUsername(username);
	 	userInput.toString();
	 	
	 	assertEquals(username, userInput.getUsername());
	 	assertEquals(name, userInput.getName());
	 	assertEquals(dealer, userInput.getDealer());
	 	assertEquals(installationCode, userInput.getInstallationCode());
	 	assertEquals(role, userInput.getRole());
	 	
	}
}
