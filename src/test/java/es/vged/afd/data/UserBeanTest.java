package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserBeanTest {
	
	@Test
	public void userBeanTest() {
		UserBean user = new UserBean();
		
		user.setDealerId(1);
		user.setId(1L);
		user.setInstallationCode("1234A");
		user.setName("name");
		user.setRole(1);
		user.setUserName("userName");
		user.toString();
		
		assertEquals(1, user.getDealer().intValue());
		assertEquals(1L, user.getId().longValue());
		assertEquals("1234A", user.getInstallationCode());
		assertEquals("name", user.getName());
		assertEquals(1, user.getRole().intValue());
		assertEquals("userName", user.getUserName());
	}

}
