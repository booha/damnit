package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProfileBeanTest {
	
	@Test
	public void ProfileBeanTest() {
		ProfileBean profile = new ProfileBean(1L, "name");
	
		profile.setId(1L);
		profile.setName("name");
		profile.toString();
		
		assertEquals(1L, profile.getId().longValue());
		assertEquals("name", profile.getName());
		
		
	}

}
