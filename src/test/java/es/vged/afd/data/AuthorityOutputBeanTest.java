package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthorityOutputBeanTest {

	@Test
	public void authorityOutputBeanTest() {
		AuthorityOutputBean authorityOutputBean = new AuthorityOutputBean();
	
		authorityOutputBean.setId(1);
		authorityOutputBean.setName("name");
		authorityOutputBean.toString();
		
		assertEquals(1, authorityOutputBean.getId().intValue());
		assertEquals("name", authorityOutputBean.getName());
	}
	
}
