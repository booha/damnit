package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.enumeration.AnnualKmEngagementFormulaEnum;

@RunWith(MockitoJUnitRunner.class)
public class UserEntityTest {
	
	@Test
	public void testUserEntity() {
		UserEntity userEntity = new UserEntity();
		DealerEntity dealer = new DealerEntity();
		
		userEntity.setDealerCode("dealerCode");
		userEntity.setDealer(dealer);
		UserEntity userEntity2 = new UserEntity(1, "username" ,"userCompleteName", new Date(), new InstallationEntity(), "dealerCode", new DealerEntity());
		userEntity.setUserProfile(new ArrayList<>());
		userEntity2.toString();
		assertEquals(dealer, userEntity.getDealer());
		assertEquals(new ArrayList<>(), userEntity.getUserProfile());
		
		AnnualKmEngagementFormulaEnum annualKmEngagementFormulaEnum;
		
	}
}
