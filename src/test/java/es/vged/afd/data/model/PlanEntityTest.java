package es.vged.afd.data.model;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanEntityTest {

	@Test
	public void testPlanEntity() {
		PlanEntity planEntity = new PlanEntity(1, "name", "desc");
		PlanEntity planEntity2 = new PlanEntity(new Date(), new Date(), "SYSTEM", "SYSTEM");
		planEntity.toString();
		planEntity2.toString();
	}
}
