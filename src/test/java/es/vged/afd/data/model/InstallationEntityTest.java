package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InstallationEntityTest {
	
	@Test
	public void testInstallationEntity() {
		DealerEntity dealer = new DealerEntity();
		
		InstallationEntity installationEntity = new InstallationEntity();
		installationEntity.setDealer(dealer);
		installationEntity.setDealerCode("dealerCode");
		
		assertEquals(dealer, installationEntity.getDealer());
		
		InstallationEntity installationEntity2 = new InstallationEntity(new Date(), new Date(), "createdBy", "lastModifiedBy");
	}

}
