package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserProfileOPKEntityTest {

	@Test
	public void UserProfileEntityPKTest() {
		UserProfileEntityPK userProfileEntityPK = new UserProfileEntityPK();
		
		UserEntity userEntity = new UserEntity();
		ProfileEntity profileEntity = new ProfileEntity();
		
		userProfileEntityPK.setProfile(profileEntity);
		userProfileEntityPK.setUser(userEntity);
		userProfileEntityPK.toString();
		
		assertEquals(profileEntity, userProfileEntityPK.getProfile());
		assertEquals(userEntity, userProfileEntityPK.getUser());
	}
}
