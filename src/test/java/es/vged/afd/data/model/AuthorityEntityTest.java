package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthorityEntityTest {
	
	@Test
	public void AuthorityEntityTest() {
		
		Integer id = 1;
		String name = "nameAuthority";
		
		AuthorityEntity authorityEntity = new AuthorityEntity();
		
		authorityEntity.setId(id);
		authorityEntity.setName(name);
		authorityEntity.setProfileAuthorities(new ArrayList<>());
		authorityEntity.toString();
		
		assertEquals(id, authorityEntity.getId());
		assertEquals(new ArrayList<>(), authorityEntity.getProfileAuthorities());
		assertEquals(name, authorityEntity.getName());
	}

}
