package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProfileEntityTest {

	@Test
	public void ProfileEntityTest() {
		
		ProfileEntity profileEntity = new ProfileEntity();
		
		profileEntity.setId(1);
		profileEntity.setName("name");
		profileEntity.setProfileAuthorities(new ArrayList<>());
		profileEntity.toString();
		
		assertEquals(1, profileEntity.getId().intValue());
		assertEquals("name", profileEntity.getName());
		assertEquals(new ArrayList<>(), profileEntity.getProfileAuthorities());
	}
}
