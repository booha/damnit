package es.vged.afd.data.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProfileAuthorityEntityTest {

	@Test
	public void ProfileAuthorityEntityTest() {
		
		ProfileAuthorityEntityPK pk = new ProfileAuthorityEntityPK();
		
		AuthorityEntity authority = new AuthorityEntity();
		ProfileEntity profileEntity = new ProfileEntity();
		
		pk.setAuthority(authority);
		pk.setProfile(profileEntity);
		
		assertEquals(authority, pk.getAuthority());
		assertEquals(profileEntity, pk.getProfile());
		
	}
}
