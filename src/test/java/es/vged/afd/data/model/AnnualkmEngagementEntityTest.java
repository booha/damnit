package es.vged.afd.data.model;

import org.junit.Assert;
import org.junit.Test;

public class AnnualkmEngagementEntityTest {

	private static final Integer CAR_AGE = 10;
	private static final Integer PAYMENT_MONTHLY = 1;

	@Test
	public void test() {

		AnnualkmEngagementEntity entity = new AnnualkmEngagementEntity();
		entity.setStartPaymentMonthly(PAYMENT_MONTHLY);
		entity.setCarAge(CAR_AGE);

		Assert.assertEquals(CAR_AGE, entity.getCarAge());
		Assert.assertEquals(PAYMENT_MONTHLY, entity.getStartPaymentMonthly());

	}

}
