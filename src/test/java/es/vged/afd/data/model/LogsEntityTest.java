package es.vged.afd.data.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LogsEntityTest {
	
	@Test
	public void testLogsEntity() {
		LogsEntity logsEntity = new LogsEntity();
		logsEntity.getId();
		logsEntity.getIdInstallation();
		logsEntity.getIdUser();
		logsEntity.getLogType();
		logsEntity.getValue();
		logsEntity.toString();
	}

}
