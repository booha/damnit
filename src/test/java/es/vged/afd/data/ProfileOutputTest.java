package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProfileOutputTest {
	
	@Test
	public void ProfileOutputTest() {
		ProfileOutput profile = new ProfileOutput(1, "name");
		ProfileOutput profile1 = new ProfileOutput();
		
		profile.setId(1);
		profile.setName("name");
		profile.toString();
		
		assertEquals(1, profile.getId().intValue());
		assertEquals("name", profile.getName());
	}

}
