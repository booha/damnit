package es.vged.afd.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DealerOutputTest {

	@Test
	public void dealerOutputTest() {
		DealerOutput dealerOutput = new DealerOutput();
		
		dealerOutput.setDealerCode("1234A");
		dealerOutput.setId(1);
		dealerOutput.setName("prueba");
		dealerOutput.toString();
		
		assertEquals("1234A", dealerOutput.getDealerCode());
		assertEquals(1, dealerOutput.getId().intValue());
		assertEquals("prueba", dealerOutput.getName());
	}
}
