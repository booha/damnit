package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ParameterEntity;

@RunWith(MockitoJUnitRunner.class)
public class ParameterEntityServiceTest {
	
	@Test
	public void parameterTest() {
		ParameterEntity parameterEntity = new ParameterEntity();
		
		int id = 1;
		String name = "name"; 
		String value = "value";
		String description = "description";
		
		parameterEntity.setDescription(description);
		parameterEntity.setId(id);
		parameterEntity.setName(name);
		parameterEntity.setValue(value);
		parameterEntity.toString();
		
		assertEquals(id, parameterEntity.getId().intValue());
		assertEquals(name, parameterEntity.getName());
		assertEquals(description, parameterEntity.getDescription());
		assertEquals(value, parameterEntity.getValue());
		
		ParameterEntity parameterEntity1 = new ParameterEntity(new Date(), new Date(), "new", "modify");
		ParameterEntity parameterEntity2 = new ParameterEntity(id, name, value, description);
	}
}
