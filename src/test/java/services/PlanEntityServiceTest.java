package services;

import static org.junit.Assert.assertEquals;

//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.PlanEntity;
//import es.vged.afd.data.model.PlanEngagementEntity;

@RunWith(MockitoJUnitRunner.class)
public class PlanEntityServiceTest {

	@Test
	public void planTest() {
		PlanEntity planEntity = new PlanEntity();
		
		int id = 1;
		String name = "name";
		String description = "description";
		int order = 1;
//		List<PlanEngagementEntity> planEngagements = new ArrayList<>();
		
		planEntity.setDescription(description);
		planEntity.setId(id);
		planEntity.setName(name);
		planEntity.setOrder(order);
		planEntity.toString();
		
		assertEquals(description, planEntity.getDescription());
		assertEquals(id, planEntity.getId().intValue());
		assertEquals(name, planEntity.getName());
		assertEquals(order, planEntity.getOrder().intValue());		
		
//		PlanEntity planEntity1 = new PlanEntity(new Date(), new Date(), "new", "modify");
//		PlanEntity planEntity2 = new PlanEntity(id, name, description);
	}
}
