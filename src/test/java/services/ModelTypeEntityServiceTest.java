package services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ModelEntity;
import es.vged.afd.data.model.ModelTypeEntity;

@RunWith(MockitoJUnitRunner.class)
public class ModelTypeEntityServiceTest {

	@Test
	public void modelTypeTest() {
		ModelTypeEntity modelTypeEntity = new ModelTypeEntity();
		
		int id = 1;
		String name = "name";
		String description = "description";
		String image = "image";
		List<ModelEntity> models = new ArrayList<>();
		
		modelTypeEntity.setDescription(description);
		modelTypeEntity.setId(id);
		modelTypeEntity.setImage(image);
		modelTypeEntity.setName(name);
		modelTypeEntity.toString();
		
		assertEquals(description, modelTypeEntity.getDescription());
		assertEquals(id, modelTypeEntity.getId().intValue());
		assertEquals(image, modelTypeEntity.getImage());
		assertEquals(name, modelTypeEntity.getName());
		
		ModelTypeEntity modelTypeEntity1 = new ModelTypeEntity(id, name, description, image);
	}
}
