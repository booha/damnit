package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import javax.persistence.Column;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ChannelEntity;

@RunWith(MockitoJUnitRunner.class)
public class ChannelEntityServiceTest {

	@Test
	public void channelTest() {

		ChannelEntity channelEntity = new ChannelEntity();
		
		int id = 1;
		String name = "name"; 
		String description = "description";
		
		channelEntity.setDescription(description);
		channelEntity.setId(id);
		channelEntity.setName(name);
	
		assertEquals(name, channelEntity.getName());
		assertEquals(id, channelEntity.getId().intValue());
		assertEquals(description, channelEntity.getDescription());
		
		ChannelEntity channelEntity1 = new ChannelEntity(new Date(), new Date(), "new", "modify");
		ChannelEntity channelEntity2 = new ChannelEntity(id, name, description);
	}
}
