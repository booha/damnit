package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ArgumentEntity;
import es.vged.afd.data.model.ChannelEntity;

@RunWith(MockitoJUnitRunner.class)
public class ArgumentEntityServiceTest {
	
	@Test
	public void argumentEntityTest() {
		ArgumentEntity argumentEntity = new ArgumentEntity();
		ChannelEntity channel = new ChannelEntity();
		channel.setId(1);
		channel.setName("name");
		argumentEntity.setChannel(channel);
		argumentEntity.setId(1);
		argumentEntity.setText("text");
		argumentEntity.setTitle("title");
		
		ArgumentEntity argumentEntity2 = new ArgumentEntity(new Date(), new Date(),"createdBy","modifyBy");
		ArgumentEntity argumentEntity3 = new ArgumentEntity(1,"title","text",new ChannelEntity());
		
		argumentEntity.toString();
		
		assertEquals("title", argumentEntity.getTitle());
		assertEquals("text", argumentEntity.getText());
		assertEquals(channel, argumentEntity.getChannel());
	
	}

}
