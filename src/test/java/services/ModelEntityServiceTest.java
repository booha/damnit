package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ModelEntity;
import es.vged.afd.data.model.ModelTypeEntity;
import es.vged.afd.data.model.SimulatorEntity;

@RunWith(MockitoJUnitRunner.class)
public class ModelEntityServiceTest {

	@Test
	public void modelTest() {
		ModelEntity modelEntity = new ModelEntity();
		
		int id = 1;
		String name = "name";
		String description = "description";
		ModelTypeEntity modelTypeEntity = new ModelTypeEntity();
		
		modelEntity.setDescription(description);
		modelEntity.setId(id);
		modelEntity.setModelTypeEntity(modelTypeEntity);
		modelEntity.setName(name);
		
		assertEquals(name, modelEntity.getName());
		assertEquals(id, modelEntity.getId().intValue());
		assertEquals(description, modelEntity.getDescription());
		assertEquals(modelTypeEntity, modelEntity.getModelTypeEntity());
		
		ModelEntity modelEntity1 = new ModelEntity(new Date(), new Date(), "new", "modify");
		ModelEntity modelEntity2 = new ModelEntity(id, name, description);

	}
}
