package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ChannelEntity;
import es.vged.afd.data.model.EngagementEntity;

@RunWith(MockitoJUnitRunner.class)
public class EngagementEntityServiceTest {

	@Test
	public void engagementTest() {
		EngagementEntity engagementEntity = new EngagementEntity();

		int id = 1;
		String name = "name";
		String description = "description";
		ChannelEntity channel = new ChannelEntity();
		
		engagementEntity.setChannel(channel);
		engagementEntity.setDescription(description);
		engagementEntity.setId(id);
		engagementEntity.setName(name);
		engagementEntity.toString();
		
		assertEquals(channel, engagementEntity.getChannel());
		assertEquals(id, engagementEntity.getId().intValue());
		assertEquals(name, engagementEntity.getName());
		assertEquals(description, engagementEntity.getDescription());
		
		EngagementEntity engagementEntity1 = new EngagementEntity(new Date(), new Date(), "new", "modify");
		EngagementEntity engagementEntity2 = new EngagementEntity(id, name, description, channel);
	}
}
