package services.common.audit;

import java.util.Arrays;
import java.util.Date;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.vged.afd.common.audit.LoggerHelper;
import es.vged.afd.common.misc.SerializationUtils;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ LoggerHelper.class, LoggerFactory.class, SerializationUtils.class })
public class LoggerHelperTest
{
	private Logger logger;

	@Before
	public void setUp() throws Exception
	{
		logger = PowerMock.createMock(Logger.class);
		PowerMock.mockStatic(LoggerFactory.class);
		EasyMock.expect(LoggerFactory.getLogger(EasyMock.anyObject(Class.class))).andReturn(logger).anyTimes();
		PowerMock.replay(LoggerFactory.class);
	}

	@Test
	public void testBeginStringArgsString()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.begin("username", "testBeginStringArgsString", new Object[] { 2L, null, "Text", new Date() });
		PowerMock.verify(logger);
	}

	@Test
	public void testBeginStringArgs()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.begin("username", "testBeginStringArgs", new Object[] { 2L, null, "Text", new Date() });
		PowerMock.verify(logger);
	}


	@Test
	public void testBeginString()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.begin("testBeginString");
		PowerMock.verify(logger);
	}

	@Test
	public void testBeginNullNullNull()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.begin("username");
		PowerMock.verify(logger);
	}

	@Test
	public void testBeginWithNoDebugEnabled()
	{
		EasyMock.expect(logger.isDebugEnabled()).andReturn(false).once();
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.begin("testBeginWithNoDebugEnabled");
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringResultStringNullLong()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringResultStringNullLong", new Date(), null, 3333L);
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringResultCollStringNullNull()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringResultCollStringNullNull", Arrays.asList("str1", "str2"), null, null);
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringNullStringExceptionLong()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringNullStringExceptionLong", null, new Exception("EndedWithException"), 3333L);
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringString()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringString");
		PowerMock.verify(logger);
	}

	@Test
	public void testEndString()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndString");
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringLong()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringLong", 444L);
		PowerMock.verify(logger);
	}

	@Test
	public void testEndNullNullNull()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end(null, null);
		PowerMock.verify(logger);
	}

	@Test
	public void testEndStringNullNullWithoutDebugEnabled()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.end("testEndStringStringWithoutDebugEnabled", null);
		PowerMock.verify(logger);
	}

	@Test
	public void testExceptionStringThrowableStringBoolean()
	{
		Exception ex = new Exception();
		int stackTraceLength = ex.getStackTrace().length;
		for (int i = 0; i <= stackTraceLength; i++)
		{
			logger.error(EasyMock.anyString());
		}
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.exception("testExceptionStringThrowableStringBoolean", ex, "Exception Message", true);
		PowerMock.verify(logger);
	}

	@Test
	public void testExceptionStringThrowableString()
	{
		Exception ex = new Exception();
		logger.error(EasyMock.anyString());
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.exception("testExceptionStringThrowableStringBoolean", ex, "Exception Message");
		PowerMock.verify(logger);
	}

	@Test
	public void testExceptionStringThrowableBoolean()
	{
		Exception ex = new Exception();
		int stackTraceLength = ex.getStackTrace().length;
		for (int i = 0; i <= stackTraceLength; i++)
		{
			logger.error(EasyMock.anyString());
		}
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.exception("testExceptionStringThrowableBoolean", ex, true);
		PowerMock.verify(logger);
	}

	@Test
	public void testExceptionStringThrowable()
	{
		logger.error(EasyMock.anyString());
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.exception("testExceptionStringThrowable", new Exception());
		PowerMock.verify(logger);
	}

	@Test
	public void testExceptionWithThrowableNull()
	{
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.exception("testExceptionWithThrowableNull", null);
	}

	@Test
	public void testDebugString()
	{
		logger.debug(EasyMock.anyString());
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true);
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.debug("testDebug");
		PowerMock.verify(logger);
	}

	@Test
	public void testDebugStringThrowable()
	{
		logger.debug(EasyMock.anyString(), EasyMock.anyObject(Throwable.class));
		PowerMock.expectLastCall();
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true);
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		Exception exception = new Exception();
		loggerHelper.debug("testDebug", exception);
		PowerMock.verify(logger);
	}

	@Test
	public void testInfoString()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.info("testInfo");
		PowerMock.verify(logger);
	}

	@Test
	public void testInfoStringThrowable()
	{
		logger.info(EasyMock.anyString(), EasyMock.anyObject(Throwable.class));
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		Exception exception = new Exception();
		loggerHelper.info("testInfo", exception);
		PowerMock.verify(logger);
	}

	@Test
	public void testWarnString()
	{
		logger.warn(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.warn("testWarn");
		PowerMock.verify(logger);
	}

	@Test
	public void testWarnStringThrowable()
	{
		logger.warn(EasyMock.anyString(), EasyMock.anyObject(Throwable.class));
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		Exception exception = new Exception();
		loggerHelper.warn("testWarn", exception);
		PowerMock.verify(logger);
	}

	@Test
	public void testErrorString()
	{
		logger.error(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.error("testError");
		PowerMock.verify(logger);
	}

	@Test
	public void testErrorStringThrowable()
	{
		logger.error(EasyMock.anyString(), EasyMock.anyObject(Throwable.class));
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		Exception exception = new Exception();
		loggerHelper.error("testError", exception);
		PowerMock.verify(logger);
	}

	@Test
	public void testIsDebugEnabled()
	{
		EasyMock.expect(logger.isDebugEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		boolean actual = loggerHelper.isDebugEnabled();
		Assert.assertTrue(actual);
		PowerMock.verify(logger);
	}

	@Test
	public void testIsInfoEnabled()
	{
		EasyMock.expect(logger.isInfoEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		boolean actual = loggerHelper.isInfoEnabled();
		Assert.assertTrue(actual);
		PowerMock.verify(logger);
	}

	@Test
	public void testIsWarnEnabled()
	{
		EasyMock.expect(logger.isWarnEnabled()).andReturn(true).once();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		boolean actual = loggerHelper.isWarnEnabled();
		Assert.assertTrue(actual);
		PowerMock.verify(logger);
	}

	@Test
	public void testCall()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.call("userName", "testCall", new Object[] { 2L, null, "Text", new Date() }, 3.5F, null, 2342L);
		PowerMock.verify(logger);
	}

	@Test
	public void testCallNullTime()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.call("userName", "testCallNullTime", new Object[] { 2L, null, "Text", new Date() }, 3.5F, null,
				null);
		PowerMock.verify(logger);
	}

	@Test
	public void testCallButError()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.call("userName", "testCallButError", new Object[] { 2L, null, "Text", new Date() }, null,
				new NumberFormatException("an error"), 2342L);
		PowerMock.verify(logger);
	}

	@Test
	public void testCallWithoutDebugEnabled()
	{
		logger.info(EasyMock.anyString());
		PowerMock.expectLastCall();
		PowerMock.replay(logger, Logger.class);
		LoggerHelper loggerHelper = LoggerHelper.getLoggerHelper(this.getClass());
		loggerHelper.call("userName", "testCallWithoutDebugEnabled", new Object[] { 2L, null, "Text", new Date() },
				3.5F, null, null);
		PowerMock.verify(logger);
	}

}