package services.common.audit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import es.vged.afd.common.audit.LogInterceptor;
import es.vged.afd.common.audit.LogInterceptorImpl;
import es.vged.afd.common.audit.LoggerHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LoggerHelper.class, LogInterceptorImpl.class })
public class LogInterceptorImplTest
{

	private LoggerHelper loggerHelper;
	private LogInterceptor logInterceptor = new LogInterceptorImpl();

	private final String methodName = "methodName";
	private final Object[] params = new Object[] { "param1", 3L };
	private final String result = "result";

	@Test
	public void testLogMethodCall() throws Throwable
	{
		this.createLoggerHelperMock();
		loggerHelper.call(EasyMock.anyObject(String.class), EasyMock.anyObject(String.class),
				EasyMock.<Object[]> anyObject(), EasyMock.anyObject(), EasyMock.anyObject(Throwable.class),
				EasyMock.anyLong());
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		ProceedingJoinPoint joinPointMock = this.getCallJoinPointMock(signatureMock);

		final String actual = (String) logInterceptor.logMethodCall(joinPointMock);

		Assert.assertEquals(result, actual);
		PowerMock.verify(signatureMock, joinPointMock, loggerHelper);
	}

	@Test(expected = NumberFormatException.class)
	public void testLogMethodCallException() throws Throwable
	{
		this.createLoggerHelperMock();
		loggerHelper.call(EasyMock.anyObject(String.class), EasyMock.anyObject(String.class),
				EasyMock.<Object[]> anyObject(), EasyMock.anyObject(), EasyMock.anyObject(Throwable.class),
				EasyMock.anyLong());
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		ProceedingJoinPoint joinPointMock = this.getCallJoinPointMockException(signatureMock);

		logInterceptor.logMethodCall(joinPointMock);
	}

	@Test
	public void testLogMethodBegin() throws Throwable
	{
		this.createLoggerHelperMock();
		loggerHelper.begin(EasyMock.anyString(), EasyMock.anyObject(String.class), EasyMock.anyObject(String.class), EasyMock.anyObject(Long.class) );
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		JoinPoint joinPointMock = this.getBeginJoinPointMock(signatureMock);

		logInterceptor.logMethodBegin(joinPointMock);

		PowerMock.verify(signatureMock, joinPointMock, loggerHelper);
	}

	@Test
	public void testLogMethodEndSuccessful() throws Throwable
	{
		this.createLoggerHelperMock();
		loggerHelper.end(EasyMock.anyObject(String.class), EasyMock.anyObject(), EasyMock.anyObject(Throwable.class),
				EasyMock.anyLong());
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		JoinPoint joinPointMock = this.getEndJoinPointMock(signatureMock);

		logInterceptor.logMethodEndSuccessful(joinPointMock, result);

		PowerMock.verify(signatureMock, joinPointMock, loggerHelper);
	}

	@Test
	public void testLogMethodEndFailure() throws Throwable
	{
		this.createLoggerHelperMock();
		loggerHelper.end(EasyMock.anyObject(String.class), EasyMock.anyObject(), EasyMock.anyObject(Throwable.class),
				EasyMock.anyLong());
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		JoinPoint joinPointMock = this.getEndJoinPointMock(signatureMock);

		logInterceptor.logMethodEndFailure(joinPointMock, new Exception("FailureTest"));

		PowerMock.verify(signatureMock, joinPointMock, loggerHelper);
	}

	@Test
	public void testLogMethodBeginAndEnd() throws Throwable
	{
		this.createLoggerHelperForBeginAndEndMock();
		loggerHelper.begin(EasyMock.anyString(), EasyMock.anyObject(String.class), EasyMock.anyObject(String.class), EasyMock.anyObject(Long.class) );
		loggerHelper.end(EasyMock.anyObject(String.class), EasyMock.anyObject(), EasyMock.anyObject(Throwable.class),
				EasyMock.anyLong());
		PowerMock.expectLastCall().anyTimes();
		PowerMock.replay(loggerHelper, LoggerHelper.class);

		Signature signatureMock = this.getSignatureMock();
		ProceedingJoinPoint joinPointMock = this.getBeginAndEndJoinPointMock(signatureMock);

		final String actual = (String) logInterceptor.logMethodBeginAndEnd(joinPointMock);

		Assert.assertEquals(result, actual);
		PowerMock.verify(signatureMock, joinPointMock, loggerHelper);
	}

	private void createLoggerHelperMock()
	{
		loggerHelper = PowerMock.createMock(LoggerHelper.class);
		PowerMock.mockStatic(LoggerHelper.class);
		EasyMock.expect(LoggerHelper.getLoggerHelper(EasyMock.anyObject(Class.class))).andReturn(loggerHelper);
	}

	private void createLoggerHelperForBeginAndEndMock()
	{
		loggerHelper = PowerMock.createMock(LoggerHelper.class);
		PowerMock.mockStatic(LoggerHelper.class);
		EasyMock.expect(LoggerHelper.getLoggerHelper(EasyMock.anyObject(Class.class))).andReturn(loggerHelper).times(2);
	}

	private Signature getSignatureMock()
	{
		Signature signatureMock = PowerMock.createMock(Signature.class);
		EasyMock.expect(signatureMock.getName()).andReturn(methodName).once();
		PowerMock.replay(signatureMock);
		return signatureMock;
	}

	private ProceedingJoinPoint getCallJoinPointMock(Signature signatureMock) throws Throwable
	{
		ProceedingJoinPoint joinPointMock = PowerMock.createMock(ProceedingJoinPoint.class);
		EasyMock.expect(joinPointMock.getTarget()).andReturn(new Object()).times(2);
		EasyMock.expect(joinPointMock.getSignature()).andReturn(signatureMock).once();
		EasyMock.expect(joinPointMock.getArgs()).andReturn(params).once();
		EasyMock.expect(joinPointMock.proceed(EasyMock.aryEq(params))).andReturn(result).once();
		PowerMock.replay(joinPointMock);
		return joinPointMock;
	}

	private ProceedingJoinPoint getCallJoinPointMockException(Signature signatureMock) throws Throwable
	{
		ProceedingJoinPoint joinPointMock = PowerMock.createMock(ProceedingJoinPoint.class);
		EasyMock.expect(joinPointMock.getTarget()).andReturn(new Object()).times(2);
		EasyMock.expect(joinPointMock.getSignature()).andReturn(signatureMock).once();
		EasyMock.expect(joinPointMock.getArgs()).andReturn(params).once();
		EasyMock.expect(joinPointMock.proceed(EasyMock.aryEq(params))).andThrow(new NumberFormatException("any error"));
		PowerMock.replay(joinPointMock);
		return joinPointMock;
	}

	private JoinPoint getBeginJoinPointMock(Signature signatureMock) throws Throwable
	{
		JoinPoint joinPointMock = PowerMock.createMock(JoinPoint.class);
		EasyMock.expect(joinPointMock.getTarget()).andReturn(new Object()).times(2);
		EasyMock.expect(joinPointMock.getSignature()).andReturn(signatureMock).once();
		EasyMock.expect(joinPointMock.getArgs()).andReturn(params).once();
		PowerMock.replay(joinPointMock);
		return joinPointMock;
	}

	private JoinPoint getEndJoinPointMock(Signature signatureMock) throws Throwable
	{
		JoinPoint joinPointMock = PowerMock.createMock(JoinPoint.class);
		EasyMock.expect(joinPointMock.getTarget()).andReturn(new Object()).times(2);
		EasyMock.expect(joinPointMock.getSignature()).andReturn(signatureMock).once();
		PowerMock.replay(joinPointMock);
		return joinPointMock;
	}

	private ProceedingJoinPoint getBeginAndEndJoinPointMock(Signature signatureMock) throws Throwable
	{
		ProceedingJoinPoint joinPointMock = PowerMock.createMock(ProceedingJoinPoint.class);
		EasyMock.expect(joinPointMock.getTarget()).andReturn(new Object()).times(3);
		EasyMock.expect(joinPointMock.getSignature()).andReturn(signatureMock).once();
		EasyMock.expect(joinPointMock.getArgs()).andReturn(params).once();
		EasyMock.expect(joinPointMock.proceed(EasyMock.aryEq(params))).andReturn(result).once();
		PowerMock.replay(joinPointMock);
		return joinPointMock;
	}

}