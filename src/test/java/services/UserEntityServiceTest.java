package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.InstallationEntity;
import es.vged.afd.data.model.UserEntity;

@RunWith(MockitoJUnitRunner.class)
public class UserEntityServiceTest {

	@Test
	public void userTest() {
		UserEntity userEntity = new UserEntity();
		
		int id = 1;
		String username = "username";
		String userCompleteName = "userCompleteName";
		Date lastDateAccess = new Date();
		InstallationEntity installation = new InstallationEntity(); 
		
		userEntity.setId(id);
		userEntity.setLastDateAccess(lastDateAccess);
		userEntity.setUserCompleteName(userCompleteName);
		userEntity.setInstallation(installation);
		userEntity.setUsername(username);
		userEntity.setInstallation(installation);
		userEntity.toString();
		
		assertEquals(id, userEntity.getId().intValue());
		assertEquals(installation, userEntity.getInstallation());
		assertEquals(lastDateAccess, userEntity.getLastDateAccess());
		assertEquals(userCompleteName, userEntity.getUserCompleteName());
		assertEquals(username, userEntity.getUsername());
		
		UserEntity userEntity1 = new UserEntity(new Date(), new Date(), "new", "modify"); 

	}
}
