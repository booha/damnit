package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.BenefitEntity;
import es.vged.afd.data.model.ChannelEntity;

@RunWith(MockitoJUnitRunner.class)
public class BenefitEntityTest {

	@Test
	public void benefitTest() {
		BenefitEntity benefitEntity = new BenefitEntity();
		ChannelEntity channel = new ChannelEntity();
		int id = 1;
		boolean entry = true;
		boolean plus = false;
		String text = "text";
		
		benefitEntity.setChannel(channel);
		benefitEntity.setEntry(entry);
		benefitEntity.setId(id);
		benefitEntity.setPlus(plus);
		benefitEntity.setText(text);
		benefitEntity.toString();
		benefitEntity.isPlus();
		assertEquals(channel, benefitEntity.getChannel());
		assertEquals(id, benefitEntity.getId());
		assertEquals(text, benefitEntity.getText());
		assertEquals(true, benefitEntity.isEntry());
		
		BenefitEntity benefitEntity1 = new BenefitEntity(id, entry, plus, text, channel);
		BenefitEntity benefitEntity2 = new BenefitEntity(new Date(), new Date(), "new", "modify");
		
	}
}
