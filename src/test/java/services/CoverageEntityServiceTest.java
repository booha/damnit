package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.ChannelEntity;
import es.vged.afd.data.model.CoverageEntity;

@RunWith(MockitoJUnitRunner.class)
public class CoverageEntityServiceTest {

	@Test
	public void coverageTest() {
		CoverageEntity coverageEntity = new CoverageEntity();
		
		int id = 1;
		String text = "text";
		boolean entry = true;
		boolean plus = true;
		String link = "link";
		ChannelEntity channel = new ChannelEntity();
		
		coverageEntity.setChannel(channel);
		coverageEntity.setEntry(entry);
		coverageEntity.setId(id);
		coverageEntity.setLink(link);
		coverageEntity.setPlus(plus);
		coverageEntity.setText(text);
		coverageEntity.isEntry();
		coverageEntity.isPlus();
		coverageEntity.toString();
		
		assertEquals(channel, coverageEntity.getChannel());
		assertEquals(id, coverageEntity.getId().intValue());
		assertEquals(link, coverageEntity.getLink());
		assertEquals(text, coverageEntity.getText());
		
		CoverageEntity coverageEntity1 = new CoverageEntity(new Date(), new Date(), "new", "modify");
		CoverageEntity coverageEntity2 = new CoverageEntity(id, text, entry, plus, link, channel);
	}
}
