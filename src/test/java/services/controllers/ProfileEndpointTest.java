package services.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.data.ProfileOutput;
import es.vged.afd.utils.UrlAPIConstants;

public class ProfileEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getAllProfilesTest() throws Exception {
		ProfileOutput profileOutput = new ProfileOutput(1, "name");
		profileOutput.toString();
		
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.PROFILE_ROOT)
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print())
					.andExpect(status().isOk()).andExpect(jsonPath("$.data").isArray())
					.andExpect(jsonPath("$.data", Matchers.hasSize(4)))
					.andExpect(jsonPath("$.data[0].name", Matchers.is("Dealer")))
					.andExpect(jsonPath("$.data[3].name", Matchers.is("Admin")));
	}
	
}
