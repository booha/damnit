package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class CoverageEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getAllCoverages() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.COVERAGES_RESOURCE + "/1")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isArray());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void getAllCoveragesByChannelAndResponseIsEmpty() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.COVERAGES_RESOURCE + "/4")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isEmpty());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
