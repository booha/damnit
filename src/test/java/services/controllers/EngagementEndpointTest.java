package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class EngagementEndpointTest extends CommonEndpointCustomTest {
	
	@Test
	public void getEngagementByChannel() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.ENGAGEMENT_RESOURCE + "/1")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isArray());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
