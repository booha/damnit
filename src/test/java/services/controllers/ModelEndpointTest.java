package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.service.StandardService;
import es.vged.afd.utils.UrlAPIConstants;

public class ModelEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getAllModels() {
		StandardService standardService = new StandardService(1, "mensaje", new ArrayList<>());
		
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.MODEL_RESOURCE)
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isArray());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
