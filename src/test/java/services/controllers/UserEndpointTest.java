package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.vged.afd.common.misc.ElapsedTime;
import es.vged.afd.data.UserBean;
import es.vged.afd.data.UserInput;
import es.vged.afd.utils.UrlAPIConstants;

public class UserEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getUserInstallation() {
		try {
			
			ElapsedTime elapsedTime = new ElapsedTime();
			elapsedTime.getElapseTime();
			
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + "/ssant01")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isNotEmpty())
					.andExpect(jsonPath("$.data", Matchers.hasSize(1)))
					.andExpect(jsonPath("$.data[0].installationCode", Matchers.is("0A1B2")))
					.andExpect(jsonPath("$.data[0].username", Matchers.is("ssant01")));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void getUserInstallationWhenUserIsNull() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + "/null")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isEmpty());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void updateLastDateAccess() {
		UserInput user = new UserInput();
		
		String username = "ssant01";
		
		user.setUsername(username);
		user.toString();
		
		try {
			mockMvc.perform(MockMvcRequestBuilders.post(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + UrlAPIConstants.USER_LAST_DATE_ACCESS_RESOURCE)
					.header("USER-JWT", userToken)
					.content(asJsonString(user))
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
					.andExpect(jsonPath("$.code").value(1));

		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckUserInstallation()  throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + UrlAPIConstants.USER_INSTALLATION)
				.header("USER-JWT", userToken)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(0));
	}
	
	private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	
	@Test
	public void getAllUsersTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + UrlAPIConstants.ALL + "/ss")
				.header("USER-JWT", userToken)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("$.data", Matchers.hasSize(1)));
	}
	
	@Test
	public void userCreateTest() throws Exception {
		UserBean userBean = new UserBean();
		
		userBean.setName("testUser");
		userBean.setUserName("Usuaio de prueba");
		userBean.setDealerId(1);
		userBean.setInstallationCode("0A1B2");
		userBean.setRole(1);
		
		mockMvc.perform(MockMvcRequestBuilders.post(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE)
				.content(asJsonString(userBean))
				.header("USER-JWT", userToken)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.data", Matchers.is(true)));
				
	}
	
	@Test
	public void userUpdateTest() throws Exception {
		UserBean userBean = new UserBean();
		userBean.setId(1L);
		userBean.setName("testUser");
		userBean.setUserName("Usuaio de prueba");
		userBean.setDealerId(1);
		userBean.setInstallationCode("0A1B2");
		userBean.setRole(1);
		
		mockMvc.perform(MockMvcRequestBuilders.post(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE)
				.content(asJsonString(userBean))
				.header("USER-JWT", userToken)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("$.data", Matchers.is(true)));
				
	}
	
	@Test
	public void deleteUserTest() throws Exception {		
		mockMvc.perform(MockMvcRequestBuilders.delete(UrlAPIConstants.API_VERSION + UrlAPIConstants.USER_RESOURCE + "/1")
				.header("USER-JWT", userToken)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(200))
				.andExpect(jsonPath("$.data", Matchers.is(true)));;
	}
}
