package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class SimulatorEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getSimulatorCalculationsWhenContractIs1_NoInstallation() throws Exception {

		final String USER_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6IkRUOE1DMEUiLCJucm8iOjYzOCwiaW5zdGFsbGF0aW9uIjp7ImNyZWF0ZWREYXRlIjoxNTQyNjI1Njg0Nzg3LCJsYXN0TW9kaWZpZWREYXRlIjoxNTQyNjI1Njg0Nzg3LCJjcmVhdGVkQnkiOiJTWVNURU0iLCJsYXN0TW9kaWZpZWRCeSI6IlNZU1RFTSIsImluc3RhbGxhdGlvbkNvZGUiOiIwMTIzRiIsImJ1c2luZXNzTmFtZSI6IklOU1RBTEFDSU9OIDAxMjNGIiwiZGVzY3JpcHRpb24iOiJMYSBkZSBGcmFuY2VzYyIsImF2ZXJhZ2VQcmljZUxhYm9yIjo3Ni4yMywiZGVhbGVyQ29kZSI6Ijk5OTkifSwiY29tcGxldGVVc2VyTmFtZSI6Ik9zY2FyIElOVEVHUkEiLCJncmFudGVkQXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IkFMTCJ9XSwiaWF0IjoxNTQ3MTM5NTIyfQ.7XENU0WtcKMnPppI3q7N9_lHGtvpEexoua9_dwV96tk"; 
		
			mockMvc.perform(MockMvcRequestBuilders
					.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE + "/4/2/1")
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andDo(print()).andExpect(status().isOk());		
	}
	
	@Test
	public void getSimulatorCalculationsWhenContractIs1() {

		try {
			mockMvc.perform(MockMvcRequestBuilders
					.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE + "/1/1/1/0310T")
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void getSimulatorCalculationsWhenContractIs2() {

		try {
			mockMvc.perform(MockMvcRequestBuilders
					.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE + "/1/2/1/0310T")
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void getSimulatorCalculationsWhenContractIs1AndKMIs2() {

		try {
			mockMvc.perform(MockMvcRequestBuilders
					.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE + "/1/1/2/0310T")
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void getSimulatorCalculationsWhenContractIs2AndKMIs2() {

		try {
			mockMvc.perform(MockMvcRequestBuilders
					.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.SIMULATOR_RESOURCE + "/1/2/2/0300X")
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
