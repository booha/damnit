package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class ArgumentEndpointTest extends CommonEndpointCustomTest {
	
	@Test
	public void getArgumentsByChannel() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.ARGUMENTS_RESOURCE + "/1")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isArray())
					.andExpect(jsonPath("$.data", Matchers.hasSize(4)))
					.andExpect(jsonPath("$.data[0].title", Matchers.is("Sin compromiso de permanencia")));
			
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void getArgumentsByChannelAndResponseIsEmpty() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.ARGUMENTS_RESOURCE + "/4")
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isEmpty());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}
