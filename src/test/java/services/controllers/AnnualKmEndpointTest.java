package services.controllers;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class AnnualKmEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getAllAnnualKm() {
		try {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.ANNUALKM_RESOURCE)
					.header("USER-JWT", userToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.data").isArray())
					.andExpect(jsonPath("$.data", Matchers.hasSize(3)))
					.andExpect(jsonPath("$.data[0].name", Matchers.is("15.000")))
					.andExpect(jsonPath("$.data[1].name", Matchers.is("22.500")));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}

