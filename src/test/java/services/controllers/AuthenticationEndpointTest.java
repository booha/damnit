package services.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.utils.UrlAPIConstants;

public class AuthenticationEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void createToken() throws Exception {
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.AUTHENTICATION_ROOT + UrlAPIConstants.AUTHENTICATION_CREATE_TOKEN)
					.header("SSO-JWT", ssoToken)
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

	}
}
