package services.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.vged.afd.data.DealerOutput;
import es.vged.afd.utils.UrlAPIConstants;

public class DealerEndpointTest extends CommonEndpointCustomTest {

	@Test
	public void getAllDealersTest() throws Exception {
		DealerOutput dealerOutput = new DealerOutput(1, "dealerCode", "name");
		dealerOutput.toString();
		
			mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.DEALER_ROOT)
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("$.data").isArray())
					.andExpect(jsonPath("$.data", Matchers.hasSize(1)))
					.andExpect(jsonPath("$.data[0].name", Matchers.is("Concesionario Ficticio")));
	}
}
