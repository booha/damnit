package services.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.vged.afd.data.InstallationInput;
import es.vged.afd.utils.UrlAPIConstants;

public class InstallationEndpointTest extends CommonEndpointCustomTest {
	
	@Test
	public void updateInstallationTest() throws Exception {
		InstallationInput installation = new InstallationInput();
		
		String installationCode = "0A1B2";
		Double averagePriceLabor = 76.23;
		String businessName = "Installation 0A1B2";
		
		installation.setBusinessName(businessName);
		installation.setInstallationCode(installationCode);
		installation.setAveragePriceLabor(averagePriceLabor);
		installation.toString();
		
		ArrayList<Object> installationList = new ArrayList<>();
 		installationList.add(installation);

 		mockMvc.perform(MockMvcRequestBuilders.post(UrlAPIConstants.API_VERSION + UrlAPIConstants.INSTALLATION_RESOURCE)
				.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
				.content(asJsonString(installationList))
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
 				.andExpect(jsonPath("$.code").value(200));
	}
	
	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testListInstallationByUser() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.INSTALLATION_RESOURCE)
				.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", Matchers.hasSize(1)))
				.andExpect(jsonPath("$.data[0].businessName", Matchers.is("I1")))
				.andExpect(jsonPath("$.data[0].installationCode", Matchers.is("0A1B2")));
	}
	
	@Test
	public void showMessageUpdateLaborPriceTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.INSTALLATION_RESOURCE + UrlAPIConstants.INSTALLATION_PRICE_MESSAGE)
 				.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
 				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
  				.andExpect(jsonPath("$.code").value(200))
  				.andExpect(jsonPath("$.data").value(false))
  				.andExpect((jsonPath("$.status").value("Success")));
 	}
 
	@Test
	public void getAllProfilesTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(UrlAPIConstants.API_VERSION + UrlAPIConstants.INSTALLATION_RESOURCE + UrlAPIConstants.ALL)
					.header("USER-JWT", userToken).accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk()).andExpect(jsonPath("$.data").isArray())
					.andExpect(jsonPath("$.data", Matchers.hasSize(1)))
					.andExpect(jsonPath("$.data[0].businessName", Matchers.is("I1")))
					.andExpect(jsonPath("$.data[0].installationCode", Matchers.is("0A1B2")));
	}
}
