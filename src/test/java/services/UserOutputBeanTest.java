package services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.DealerOutput;
import es.vged.afd.data.InstallationOutBean;
import es.vged.afd.data.ProfileBean;
import es.vged.afd.data.UserOutputBean;

@RunWith(MockitoJUnitRunner.class)
public class UserOutputBeanTest {
	
	@Test
	public void userOutputBeanTest() {
		UserOutputBean userInput = new UserOutputBean();
		
		String username = "userName";
	 	String name = "name";
	 	DealerOutput dealer = new DealerOutput();
	 	InstallationOutBean installation = new InstallationOutBean();
	 	ProfileBean role = new ProfileBean();
	 	
	 	userInput.setDealer(dealer);
	 	userInput.setInstallation(installation);
	 	userInput.setName(name);
	 	userInput.setRole(role);
	 	userInput.setUserName(username);
	 	userInput.toString();
	 	
	 	assertEquals(username, userInput.getUserName());
	 	assertEquals(name, userInput.getName());
	 	assertEquals(dealer, userInput.getDealer());
	 	assertEquals(installation, userInput.getInstallation());
	 	assertEquals(role, userInput.getRole());
	}

}
