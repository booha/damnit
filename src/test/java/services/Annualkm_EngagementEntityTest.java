package services;

import static org.junit.Assert.assertEquals;

//import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.AnnualkmEntity;
import es.vged.afd.data.model.AnnualkmEngagementEntity;
import es.vged.afd.data.model.EngagementEntity;
//import es.vged.afd.data.model.enumeration.AnnualKmEngagementFormulaEnum;
import es.vged.afd.data.model.enumeration.AnnualKmEngagementFormulaEnum;

@RunWith(MockitoJUnitRunner.class)
public class Annualkm_EngagementEntityTest {

	@Test
	public void annualkmEngagementEntityTest() {
		AnnualkmEngagementEntity annualkm_EngagementEntity = new AnnualkmEngagementEntity();
		AnnualkmEntity annualkm = new AnnualkmEntity();
		EngagementEntity engagement = new EngagementEntity();
		annualkm_EngagementEntity.setAnnualkm(annualkm);
		annualkm_EngagementEntity.setDescription(AnnualKmEngagementFormulaEnum.SAVINGS_PV.toString());
		annualkm_EngagementEntity.setEngagement(engagement);
		annualkm_EngagementEntity.setFormula("formula");
		annualkm_EngagementEntity.setId(1);
		annualkm_EngagementEntity.setLongTermVar(1);
		annualkm_EngagementEntity.setShortTermVar(1);

//		Annualkm_EngagementEntity annualkm_EngagementEntity2 = new Annualkm_EngagementEntity(new Date(), new Date(),
//				"create", "modify");
		
//		Annualkm_EngagementEntity annualkm_EngagementEntity3 = new Annualkm_EngagementEntity(1, 1, 1, 1, 1, AnnualKmEngagementFormulaEnum.SAVINGS_PV.toString(), "desc", annualkm,
//				engagement);

		assertEquals(annualkm, annualkm_EngagementEntity.getAnnualkm());
		assertEquals(AnnualKmEngagementFormulaEnum.SAVINGS_PV.toString(), annualkm_EngagementEntity.getDescription());
		assertEquals(engagement, annualkm_EngagementEntity.getEngagement());
		assertEquals("formula", annualkm_EngagementEntity.getFormula());
		assertEquals(1, annualkm_EngagementEntity.getId().intValue());
		assertEquals(1, annualkm_EngagementEntity.getLongTermVar().intValue());
		assertEquals(1, annualkm_EngagementEntity.getShortTermVar().intValue());
		annualkm_EngagementEntity.toString();
	}

}
