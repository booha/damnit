package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

//import java.sql.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.AnnualkmEntity;
import es.vged.afd.data.model.ModelEntity;
import es.vged.afd.data.model.PlanEngagementEntity;
import es.vged.afd.data.model.SimulatorEntity;

@RunWith(MockitoJUnitRunner.class)
public class SimulatorEntityTest {

	@Test
	public void simulatorTest() {
		SimulatorEntity simulatorEntity = new SimulatorEntity();
		int id = 1;
		double feeAfd = 1.2;
		double longCost = 1.0;
		double shortCost = 1.1;
		int monthCar = 1;
		double laborPercentage = 1.1;
		PlanEngagementEntity planEngagement = new PlanEngagementEntity();
		ModelEntity model = new ModelEntity();
		AnnualkmEntity annualkm = new AnnualkmEntity();
		SimulatorEntity simulatorEntity2 = new SimulatorEntity(new Date(), new Date(), "SYSTEM", "SYSTEM");
		
		simulatorEntity.setAnnualkm(annualkm);
		simulatorEntity.setFeeAfd(feeAfd);
		simulatorEntity.setId(id);
		simulatorEntity.setLaborPercentage(laborPercentage);
		simulatorEntity.setLongCost(longCost);
		simulatorEntity.setModel(model);
		simulatorEntity.setMonthCar(monthCar);
		simulatorEntity.setPlanengagement(planEngagement);
		simulatorEntity.setShortCost(shortCost);
		simulatorEntity.toString();
		
		assertEquals(annualkm, simulatorEntity.getAnnualkm());
		assertEquals(feeAfd, simulatorEntity.getFeeAfd().doubleValue(), 1.2);
		assertEquals(id, simulatorEntity.getId().intValue());
		assertEquals(laborPercentage, simulatorEntity.getLaborPercentage().doubleValue(),1.1);
		assertEquals(longCost, simulatorEntity.getLongCost().doubleValue(),1.0);
		assertEquals(model, simulatorEntity.getModel());
		assertEquals(monthCar, simulatorEntity.getMonthCar().intValue());
		assertEquals(planEngagement, simulatorEntity.getPlanengagement());
		assertEquals(shortCost, simulatorEntity.getShortCost().doubleValue(),1.1);
		
		
//		SimulatorEntity simulatorEntity1 = new SimulatorEntity(new Date(monthCar), new Date(monthCar), "created", "modify");
//		SimulatorEntity simulatorEntity2 = new SimulatorEntity(id, feeAfd, longCost, shortCost, monthCar,  laborPercentage, planEngagement, annualkm, model);
	}
}
