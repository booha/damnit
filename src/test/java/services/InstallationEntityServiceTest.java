package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.DealerEntity;
import es.vged.afd.data.model.InstallationEntity;

@RunWith(MockitoJUnitRunner.class)
public class InstallationEntityServiceTest {

	@Test
	public void installationTest() {
		InstallationEntity installationEntity = new InstallationEntity();

		String id = "02022";
		String name = "name";
		String description = "description";
		double averagePriceLabor = 1.0;

		installationEntity.setAveragePriceLabor(averagePriceLabor);
		installationEntity.setDescription(description);
		installationEntity.setInstallationCode(id);
		installationEntity.setBusinessName(name);
		installationEntity.setDealer(new DealerEntity());

		assertEquals(averagePriceLabor, installationEntity.getAveragePriceLabor().doubleValue(), 1.0);
		assertEquals(id, installationEntity.getInstallationCode());
		assertEquals(description, installationEntity.getDescription());
		assertEquals(name, installationEntity.getBusinessName());

		InstallationEntity installationEntity2 = new InstallationEntity(id, name, description, averagePriceLabor);
	}
}
