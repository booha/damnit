package services.security;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;

import es.vged.afd.data.model.InstallationEntity;
import es.vged.afd.service.security.UserJWT;


@RunWith(MockitoJUnitRunner.class)
public class UserJWTTest {

	@Test
	public void TokenEntityTest() {
		InstallationEntity installationEntity = new InstallationEntity();
		List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
		
		UserJWT token = new UserJWT(1L,"username",grantedAuthorityList);
				
		token.setName("name");		
		token.setInstallation(installationEntity);
		
		
		assertEquals(grantedAuthorityList, token.getAuthorities());
		assertEquals("name", token.getName());
		//assertEquals("nnstalation", token.getInstallation());
		assertEquals(1L, token.getId().longValue());
		assertEquals("username", token.getUsername());
		assertEquals(installationEntity, token.getInstallation());
		token.toString();
	}
}
