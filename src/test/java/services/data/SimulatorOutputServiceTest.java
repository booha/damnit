package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.SimulatorCalculationsOutput;
import es.vged.afd.data.SimulatorOutput;

@RunWith(MockitoJUnitRunner.class)
public class SimulatorOutputServiceTest {

	@Test
	public void simulatorOutput () {
		
		SimulatorOutput simulatorOutput = new SimulatorOutput();
		
		int planId = 1;
		String planName = "planName";
		SimulatorCalculationsOutput simulatorCalculations = new SimulatorCalculationsOutput();
		
		simulatorOutput.setPlanId(planId);
		simulatorOutput.setPlanName(planName);
		simulatorOutput.setSimulatorCalculations(simulatorCalculations);
		simulatorOutput.toString();
		
		assertEquals(planId, simulatorOutput.getPlanId().intValue());
		assertEquals(planName, simulatorOutput.getPlanName());
		assertEquals(simulatorCalculations, simulatorOutput.getSimulatorCalculations());
		
		SimulatorOutput simulatorOutput1 = new SimulatorOutput(planId, planName, simulatorCalculations);
		
	}
}
