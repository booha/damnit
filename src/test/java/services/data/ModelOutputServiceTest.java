package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.ModelOutput;

@RunWith(MockitoJUnitRunner.class)
public class ModelOutputServiceTest {

	@Test
	public void modelOutputTest() {

		ModelOutput modelOutput = new ModelOutput();

		String modelName = "modelName";
		int submodelId = 1;
	    String submodelName = "submodelName";
	    String submodelImage = "submodelImage";
	    
	    modelOutput.setModelName(submodelName);
	    modelOutput.setSubmodelId(submodelId);
	    modelOutput.setSubmodelImage(submodelImage);
	    modelOutput.setSubmodelName(submodelName);
	    modelOutput.toString();
	    
//	    assertEquals(modelName, modelOutput.getModelName());
	    assertEquals(submodelId, modelOutput.getSubmodelId().intValue());
	    assertEquals(submodelName, modelOutput.getSubmodelName());
	    assertEquals(submodelImage, modelOutput.getSubmodelImage());
	    
	    ModelOutput modelOutput1 = new ModelOutput(modelName, submodelId, submodelName, submodelImage);
	}
}
