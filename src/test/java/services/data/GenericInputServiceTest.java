package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.GenericInput;

@RunWith(MockitoJUnitRunner.class)
public class GenericInputServiceTest {

	@Test
	public void genericInputTest() {
		
		GenericInput genericInput = new GenericInput();
		
		int id = 1;
		String name = "name";
		
		genericInput.setId(id);
		genericInput.setName(name);
		genericInput.toString();
		
		assertEquals(id, genericInput.getId().intValue());
		assertEquals(name,  genericInput.getName());
		
		GenericInput genericInput1 = new GenericInput(id, name);
	}
}
