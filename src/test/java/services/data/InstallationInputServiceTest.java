package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.InstallationInput;

@RunWith(MockitoJUnitRunner.class)
public class InstallationInputServiceTest {

	@Test
	public void installationInputTest() {
		InstallationInput installationInput = new InstallationInput();
		
		String installationCode = "02022";
		double priceLabor = 1.0;
		
		installationInput.setAveragePriceLabor(priceLabor);
		installationInput.setInstallationCode(installationCode);
		installationInput.toString();
		
		assertEquals(priceLabor, installationInput.getAveragePriceLabor().doubleValue(),1.0);
		assertEquals(installationCode, installationInput.getInstallationCode());
		
		InstallationInput installationInput1 = new InstallationInput(installationCode, installationCode, priceLabor);
	}
}
