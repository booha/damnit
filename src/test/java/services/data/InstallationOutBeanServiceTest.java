package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.InstallationOutBean;

@RunWith(MockitoJUnitRunner.class)
public class InstallationOutBeanServiceTest {
	@Test
	public void installationOutputBeanTest() {
		
		InstallationOutBean installationOutBean = new InstallationOutBean();
		
		String installationCode = "20CXG";
		String businessName = "Instalación 20CXG";
		double avgPrice = 17.99;
		
		installationOutBean.setAvgPrice(avgPrice);
		installationOutBean.setBusinessName(businessName);
		installationOutBean.setInstallationCode(installationCode);
		installationOutBean.toString();
		
		assertEquals(installationCode, installationOutBean.getInstallationCode());
		assertEquals(businessName, installationOutBean.getBusinessName());
		assertEquals(avgPrice, installationOutBean.getAvgPrice().doubleValue(), 17.99);
		
		InstallationOutBean installationOutBean2 = new InstallationOutBean(installationCode, businessName, avgPrice);
	}
}
