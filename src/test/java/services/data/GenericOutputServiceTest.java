package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.GenericOutput;

@RunWith(MockitoJUnitRunner.class)
public class GenericOutputServiceTest {

	@Test
	public void genericOutputTest() {
		
		GenericOutput genericOutput = new GenericOutput();
		
		int id = 1;
		String name = "name";
		
		genericOutput.setId(id);
		genericOutput.setName(name);
		genericOutput.toString();
		
		assertEquals(id, genericOutput.getId().intValue());
		assertEquals(name, genericOutput.getName());
		
		GenericOutput genericOutput1 = new GenericOutput(id, name);

	}
}
