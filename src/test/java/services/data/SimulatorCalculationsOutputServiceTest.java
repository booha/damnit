package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.SimulatorCalculationsOutput;

@RunWith(MockitoJUnitRunner.class)
public class SimulatorCalculationsOutputServiceTest {

	@Test
	public void simulatorCalculationsOutputTest() {
		SimulatorCalculationsOutput simulatorCalculationsOutput = new SimulatorCalculationsOutput();
		
		double feeAfd = 1.0;
		double longCost = 1.1;
		double shortCost = 1.2;
		int monthPromotion = 1;
		double copayment = 1.3;
		String longTermSavings = "longTermSavings"; 
		String shortTermSavings = "shortTermSavings";
		
		simulatorCalculationsOutput.setCopayment(copayment);
		simulatorCalculationsOutput.setFeeAfd(feeAfd);
		simulatorCalculationsOutput.setLongCost(longCost);
		simulatorCalculationsOutput.setLongTermSavings(longTermSavings);
		simulatorCalculationsOutput.setMonthPromotion(monthPromotion);
		simulatorCalculationsOutput.setShortCost(shortCost);
		simulatorCalculationsOutput.setShortTermSavings(shortTermSavings);
		simulatorCalculationsOutput.toString();
		
		assertEquals(copayment, simulatorCalculationsOutput.getCopayment().doubleValue(),1.3);
		assertEquals(feeAfd, simulatorCalculationsOutput.getFeeAfd().doubleValue(),1.0);
		assertEquals(longCost, simulatorCalculationsOutput.getLongCost().doubleValue(),1.1);
		assertEquals(longTermSavings, simulatorCalculationsOutput.getLongTermSavings());
		assertEquals(monthPromotion, simulatorCalculationsOutput.getMonthPromotion().intValue());
		assertEquals(shortCost, simulatorCalculationsOutput.getShortCost().doubleValue(),1.2);
		assertEquals(shortTermSavings, simulatorCalculationsOutput.getShortTermSavings());
			
		SimulatorCalculationsOutput simulatorCalculationsOutput1 = new SimulatorCalculationsOutput(feeAfd, longCost, shortCost, monthPromotion, copayment, longTermSavings, shortTermSavings);
	}
}
