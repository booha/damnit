package services.data;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.AuthorityOutputBean;
import es.vged.afd.data.UserInstallationOutput;

@RunWith(MockitoJUnitRunner.class)
public class UserInstallationOutputServiceTest {
	
	@Test
	public void userInstallationOutputTest() {
		UserInstallationOutput userInstallationOutput = new UserInstallationOutput();
		
		String username = "username";
		String user_name = "user_name";
		String installationId = "02022";
		String installationName = "installationName";
		double installationLaborPrice = 1.0;
		String role = "admin";
		List<String> authorities = new ArrayList<>();
		
		userInstallationOutput.setInstallationCode(installationId);
		userInstallationOutput.setInstallationLaborPrice(installationLaborPrice);
		userInstallationOutput.setInstallationName(installationName);
		userInstallationOutput.setUserCompleteName(user_name);
		userInstallationOutput.setUsername(username);
		userInstallationOutput.setRole(role);
		userInstallationOutput.setAuthorities(authorities);
		userInstallationOutput.toString();
		
		assertEquals(installationId, userInstallationOutput.getInstallationCode());
		assertEquals(installationLaborPrice, userInstallationOutput.getInstallationLaborPrice().doubleValue(),1.0);
		assertEquals(installationName, userInstallationOutput.getInstallationName());
		assertEquals(user_name, userInstallationOutput.getUserCompleteName());
		assertEquals(username, userInstallationOutput.getUsername());
		assertEquals(role, userInstallationOutput.getRole());
		assertEquals(authorities, userInstallationOutput.getAuthorities());

	}
	
}
