package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.UserInput;

@RunWith(MockitoJUnitRunner.class)
public class UserInputServiceTest {

	@Test
	public void userInputTest() {
		
		UserInput userInput = new UserInput();

		String username = "username";
		
		userInput.setUsername(username);
		userInput.toString();
		
		assertEquals(username, userInput.getUsername());
		
		UserInput userInput1 = new UserInput(username);
	}
}
