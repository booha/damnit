package services.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.SimulatorInput;

@RunWith(MockitoJUnitRunner.class)
public class SimulatorInputServiceTest {

	@Test
	public void simulatorInputTest() {
		
		SimulatorInput simulatorInput = new SimulatorInput();
		
		int model = 1;
		int annualKm = 2;
		int engagement = 3;
		
		simulatorInput.setAnnualKm(annualKm);
		simulatorInput.setEngagement(engagement);
		simulatorInput.setModel(model);
		simulatorInput.toString();
		
		assertEquals(annualKm, simulatorInput.getAnnualKm().intValue());
		assertEquals(model, simulatorInput.getModel().intValue());
		assertEquals(engagement, simulatorInput.getEngagement().intValue());
		
		SimulatorInput simulatorInput1 = new SimulatorInput(model, annualKm, engagement);
		
	}
}
