package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.AnnualkmEntity;

@RunWith(MockitoJUnitRunner.class)
public class AnnualkmEntityTest {
	
	@Test
	public void annualKmEntityTest() {
		AnnualkmEntity annualkmEntity = new AnnualkmEntity();
		annualkmEntity.setId(1);
		annualkmEntity.setName("name");
		annualkmEntity.setDescription("description");
		annualkmEntity.toString();
		
		assertEquals(1, annualkmEntity.getId().longValue());
		assertEquals("name", annualkmEntity.getName());
		assertEquals("description", annualkmEntity.getDescription());
		AnnualkmEntity annualkmEntity2 = new AnnualkmEntity(new Date(), new Date(),"creator","modified");
		AnnualkmEntity annualkmEntity3 = new AnnualkmEntity(1,"name","description");
		annualkmEntity.toString();
	}

}
