package services;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.AuditableEntity;

@RunWith(MockitoJUnitRunner.class)
public class AuditableEntityTest {

	@Test
	public void auditableTest() {
		AuditableEntity auditableEntity = new AuditableEntity();
		Date createdDate = new Date();
		Date lastModifiedDate = new Date();
		String createdBy = "createdBy";
		String lastModifiedBy = "lastModifiedBy";
		
		auditableEntity.setCreatedBy(createdBy);
		auditableEntity.setCreatedDate(createdDate);
		auditableEntity.setLastModifiedBy(lastModifiedBy);
		auditableEntity.setLastModifiedDate(lastModifiedDate);
		auditableEntity.toString();
		
		assertEquals(createdBy, auditableEntity.getCreatedBy());
		assertEquals(createdDate, auditableEntity.getCreatedDate());
		assertEquals(lastModifiedBy, auditableEntity.getLastModifiedBy());
		assertEquals(lastModifiedDate, auditableEntity.getLastModifiedDate());
		
	}
}
