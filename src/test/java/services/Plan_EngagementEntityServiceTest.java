package services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.data.model.EngagementEntity;
import es.vged.afd.data.model.PlanEntity;
import es.vged.afd.data.model.PlanEngagementEntity;

@RunWith(MockitoJUnitRunner.class)
public class Plan_EngagementEntityServiceTest {

	@Test
	public void plan_EngagementTest() {
		PlanEngagementEntity planEngagementEntity = new PlanEngagementEntity();
		
		int id = 1;
		int monthPromotion = 2;
		double copayment = 1.0;
		EngagementEntity engagement = new EngagementEntity();
		PlanEntity plan = new PlanEntity();
		
		planEngagementEntity.setCopayment(copayment);
		planEngagementEntity.setEngagement(engagement);
		planEngagementEntity.setId(id);
		planEngagementEntity.setMonthpromotion(monthPromotion);
		planEngagementEntity.setPlan(plan);
		planEngagementEntity.toString();
		
		assertEquals(copayment, planEngagementEntity.getCopayment().doubleValue(),1.0);
		assertEquals(engagement, planEngagementEntity.getEngagement());
		assertEquals(id, planEngagementEntity.getId().intValue());
		assertEquals(monthPromotion, planEngagementEntity.getMonthpromotion().intValue());
		assertEquals(plan, planEngagementEntity.getPlan());
		
//		PlanEngagementEntity planEngagementEntity1 = new PlanEngagementEntity(new Date(), new Date(), "new", "modify");
//		PlanEngagementEntity planEngagementEntity2 = new PlanEngagementEntity(id, monthPromotion, copayment, engagement, plan);
	}
}
