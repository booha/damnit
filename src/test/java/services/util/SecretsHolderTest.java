package services.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import es.vged.afd.config.SecretsHolder;

@RunWith(MockitoJUnitRunner.class)
public class SecretsHolderTest {
	
	@Test
	public void SecretsHolderEntityTest() {
		SecretsHolder secretsHolder = new SecretsHolder();
		secretsHolder.setPassword("pass");
		secretsHolder.setUsername("username");
		
		assertEquals("pass",secretsHolder.getPassword());
		assertEquals("username", secretsHolder.getUsername());
	}

}
